let covid = {
    "Afghanistan": {
        "All": {
            "confirmed": 64575,
            "recovered": 55687,
            "deaths": 2772,
            "country": "Afghanistan",
            "population": 35530081,
            "sq_km_area": 652090,
            "life_expectancy": "45.9",
            "elevation_in_meters": null,
            "continent": "Asia",
            "abbreviation": "AF",
            "location": "Southern and Central Asia",
            "iso": 4,
            "capital_city": "Kabul",
            "lat": "33.93911",
            "long": "67.709953",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Albania": {
        "All": {
            "confirmed": 132118,
            "recovered": 127869,
            "deaths": 2440,
            "country": "Albania",
            "population": 2930187,
            "sq_km_area": 28748,
            "life_expectancy": "71.6",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "AL",
            "location": "Southern Europe",
            "iso": 8,
            "capital_city": "Tirana",
            "lat": "41.1533",
            "long": "20.1683",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Algeria": {
        "All": {
            "confirmed": 125896,
            "recovered": 87746,
            "deaths": 3395,
            "country": "Algeria",
            "population": 41318142,
            "sq_km_area": 2381741,
            "life_expectancy": "69.7",
            "elevation_in_meters": 800,
            "continent": "Africa",
            "abbreviation": "DZ",
            "location": "Northern Africa",
            "iso": 12,
            "capital_city": "Alger",
            "lat": "28.0339",
            "long": "1.6596",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Andorra": {
        "All": {
            "confirmed": 13569,
            "recovered": 13234,
            "deaths": 127,
            "country": "Andorra",
            "population": 76965,
            "sq_km_area": 468,
            "life_expectancy": "83.5",
            "elevation_in_meters": "1,996",
            "continent": "Europe",
            "abbreviation": "AD",
            "location": "Southern Europe",
            "iso": 20,
            "capital_city": "Andorra la Vella",
            "lat": "42.5063",
            "long": "1.5218",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Angola": {
        "All": {
            "confirmed": 31438,
            "recovered": 26458,
            "deaths": 696,
            "country": "Angola",
            "population": 29784193,
            "sq_km_area": 1246700,
            "life_expectancy": "38.3",
            "elevation_in_meters": "1,112",
            "continent": "Africa",
            "abbreviation": "AO",
            "location": "Central Africa",
            "iso": 24,
            "capital_city": "Luanda",
            "lat": "-11.2027",
            "long": "17.8739",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Antigua and Barbuda": {
        "All": {
            "confirmed": 1255,
            "recovered": 1188,
            "deaths": 42,
            "country": "Antigua and Barbuda",
            "population": 102012,
            "sq_km_area": 442,
            "life_expectancy": "70.5",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "AG",
            "location": "Caribbean",
            "iso": 28,
            "capital_city": "Saint John's",
            "lat": "17.0608",
            "long": "-61.7964",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Argentina": {
        "All": {
            "confirmed": 3411160,
            "recovered": 3009150,
            "deaths": 72265,
            "country": "Argentina",
            "population": 44271041,
            "sq_km_area": 2780400,
            "life_expectancy": "75.1",
            "elevation_in_meters": 595,
            "continent": "South America",
            "abbreviation": "AR",
            "location": "South America",
            "iso": 32,
            "capital_city": "Buenos Aires",
            "lat": "-38.4161",
            "long": "-63.6167",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Armenia": {
        "All": {
            "confirmed": 221559,
            "recovered": 209980,
            "deaths": 4364,
            "country": "Armenia",
            "population": 2930450,
            "sq_km_area": 29800,
            "life_expectancy": "66.4",
            "elevation_in_meters": "1,792",
            "continent": "Asia",
            "abbreviation": "AM",
            "location": "Middle East",
            "iso": 51,
            "capital_city": "Yerevan",
            "lat": "40.0691",
            "long": "45.0382",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Australia": {
        "All": {
            "confirmed": 29997,
            "recovered": 23521,
            "deaths": 910,
            "country": "Australia",
            "population": 24450561,
            "sq_km_area": 7741220,
            "life_expectancy": "79.8",
            "elevation_in_meters": 330,
            "continent": "Oceania",
            "abbreviation": "AU",
            "location": "Australia and New Zealand",
            "iso": 36,
            "capital_city": "Canberra"
        },
        "Australian Capital Territory": {
            "lat": "-35.4735",
            "long": "149.0124",
            "confirmed": 124,
            "recovered": 121,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "New South Wales": {
            "lat": "-33.8688",
            "long": "151.2093",
            "confirmed": 5567,
            "recovered": 0,
            "deaths": 54,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Northern Territory": {
            "lat": "-12.4634",
            "long": "130.8456",
            "confirmed": 170,
            "recovered": 165,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Queensland": {
            "lat": "-27.4698",
            "long": "153.0251",
            "confirmed": 1593,
            "recovered": 1571,
            "deaths": 7,
            "updated": "2021/05/20 17:21:11+00"
        },
        "South Australia": {
            "lat": "-34.9285",
            "long": "138.6007",
            "confirmed": 747,
            "recovered": 736,
            "deaths": 4,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tasmania": {
            "lat": "-42.8821",
            "long": "147.3272",
            "confirmed": 234,
            "recovered": 221,
            "deaths": 13,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Victoria": {
            "lat": "-37.8136",
            "long": "144.9631",
            "confirmed": 20546,
            "recovered": 19707,
            "deaths": 820,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Western Australia": {
            "lat": "-31.9505",
            "long": "115.8605",
            "confirmed": 1016,
            "recovered": 1000,
            "deaths": 9,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Austria": {
        "All": {
            "confirmed": 639616,
            "recovered": 619756,
            "deaths": 10527,
            "country": "Austria",
            "population": 8735453,
            "sq_km_area": 83859,
            "life_expectancy": "77.7",
            "elevation_in_meters": 910,
            "continent": "Europe",
            "abbreviation": "AT",
            "location": "Western Europe",
            "iso": 40,
            "capital_city": "Wien",
            "lat": "47.5162",
            "long": "14.5501",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Azerbaijan": {
        "All": {
            "confirmed": 331477,
            "recovered": 318502,
            "deaths": 4828,
            "country": "Azerbaijan",
            "population": 9827589,
            "sq_km_area": 86600,
            "life_expectancy": "62.9",
            "elevation_in_meters": 384,
            "continent": "Asia",
            "abbreviation": "AZ",
            "location": "Middle East",
            "iso": 31,
            "capital_city": "Baku",
            "lat": "40.1431",
            "long": "47.5769",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bahamas": {
        "All": {
            "confirmed": 11396,
            "recovered": 10221,
            "deaths": 222,
            "country": "Bahamas",
            "population": 395361,
            "sq_km_area": 13878,
            "life_expectancy": "71.1",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "BS",
            "location": "Caribbean",
            "iso": 44,
            "capital_city": "Nassau",
            "lat": "25.025885",
            "long": "-78.035889",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bahrain": {
        "All": {
            "confirmed": 206878,
            "recovered": 188496,
            "deaths": 773,
            "country": "Bahrain",
            "population": 1492584,
            "sq_km_area": 694,
            "life_expectancy": 73,
            "elevation_in_meters": null,
            "continent": "Asia",
            "abbreviation": "BH",
            "location": "Middle East",
            "iso": 48,
            "capital_city": "al-Manama",
            "lat": "26.0275",
            "long": "50.55",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bangladesh": {
        "All": {
            "confirmed": 785194,
            "recovered": 727510,
            "deaths": 12284,
            "country": "Bangladesh",
            "population": 164669751,
            "sq_km_area": 143998,
            "life_expectancy": "60.2",
            "elevation_in_meters": 85,
            "continent": "Asia",
            "abbreviation": "BD",
            "location": "Southern and Central Asia",
            "iso": 50,
            "capital_city": "Dhaka",
            "lat": "23.685",
            "long": "90.3563",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Barbados": {
        "All": {
            "confirmed": 3975,
            "recovered": 3922,
            "deaths": 47,
            "country": "Barbados",
            "population": 285719,
            "sq_km_area": 430,
            "life_expectancy": 73,
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "BB",
            "location": "Caribbean",
            "iso": 52,
            "capital_city": "Bridgetown",
            "lat": "13.1939",
            "long": "-59.5432",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Belarus": {
        "All": {
            "confirmed": 381546,
            "recovered": 371666,
            "deaths": 2742,
            "country": "Belarus",
            "population": 9468338,
            "sq_km_area": 207600,
            "life_expectancy": 68,
            "elevation_in_meters": 160,
            "continent": "Europe",
            "abbreviation": "BY",
            "location": "Eastern Europe",
            "iso": 112,
            "capital_city": "Minsk",
            "lat": "53.7098",
            "long": "27.9534",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Belgium": {
        "All": {
            "confirmed": 1038514,
            "recovered": 0,
            "deaths": 24768,
            "country": "Belgium",
            "population": 11429336,
            "sq_km_area": 30518,
            "life_expectancy": "77.8",
            "elevation_in_meters": 181,
            "continent": "Europe",
            "abbreviation": "BE",
            "location": "Western Europe",
            "iso": 56,
            "capital_city": "Bruxelles [Brussel]"
        },
        "Antwerp": {
            "lat": "51.2195",
            "long": "4.4024",
            "confirmed": 134059,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Brussels": {
            "lat": "50.8503",
            "long": "4.3517",
            "confirmed": 127493,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "East Flanders": {
            "lat": "51.0362",
            "long": "3.7373",
            "confirmed": 119490,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Flemish Brabant": {
            "lat": "50.9167",
            "long": "4.5833",
            "confirmed": 79028,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hainaut": {
            "lat": "50.5257",
            "long": "4.0621",
            "confirmed": 153178,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Liege": {
            "lat": "50.4496",
            "long": "5.8492",
            "confirmed": 125857,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Limburg": {
            "lat": "50.9739",
            "long": "5.342",
            "confirmed": 58283,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Luxembourg": {
            "lat": "50.0547",
            "long": "5.4677",
            "confirmed": 30299,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Namur": {
            "lat": "50.331",
            "long": "4.8221",
            "confirmed": 57685,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 19761,
            "recovered": 0,
            "deaths": 24768,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Walloon Brabant": {
            "lat": "50.4",
            "long": "4.35",
            "confirmed": 39580,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "West Flanders": {
            "lat": "51.0536",
            "long": "3.1458",
            "confirmed": 93801,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Belize": {
        "All": {
            "confirmed": 12753,
            "recovered": 12346,
            "deaths": 323,
            "country": "Belize",
            "population": 374681,
            "sq_km_area": 22696,
            "life_expectancy": "70.9",
            "elevation_in_meters": 173,
            "continent": "North America",
            "abbreviation": "BZ",
            "location": "Central America",
            "iso": 84,
            "capital_city": "Belmopan",
            "lat": "17.1899",
            "long": "-88.4976",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Benin": {
        "All": {
            "confirmed": 8025,
            "recovered": 7893,
            "deaths": 101,
            "country": "Benin",
            "population": 11175692,
            "sq_km_area": 112622,
            "life_expectancy": "50.2",
            "elevation_in_meters": 273,
            "continent": "Africa",
            "abbreviation": "BJ",
            "location": "Western Africa",
            "iso": 204,
            "capital_city": "Porto-Novo",
            "lat": "9.3077",
            "long": "2.3158",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bhutan": {
        "All": {
            "confirmed": 1338,
            "recovered": 1186,
            "deaths": 1,
            "country": "Bhutan",
            "population": 807610,
            "sq_km_area": 47000,
            "life_expectancy": "52.4",
            "elevation_in_meters": "3,280",
            "continent": "Asia",
            "abbreviation": "BT",
            "location": "Southern and Central Asia",
            "iso": 64,
            "capital_city": "Thimphu",
            "lat": "27.5142",
            "long": "90.4336",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bolivia": {
        "All": {
            "confirmed": 340207,
            "recovered": 276549,
            "deaths": 13693,
            "country": "Bolivia",
            "population": 11051600,
            "sq_km_area": 1098581,
            "life_expectancy": "63.7",
            "elevation_in_meters": "1,192",
            "continent": "South America",
            "abbreviation": "BO",
            "location": "South America",
            "iso": 68,
            "capital_city": "La Paz",
            "lat": "-16.2902",
            "long": "-63.5887",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bosnia and Herzegovina": {
        "All": {
            "confirmed": 203018,
            "recovered": 171332,
            "deaths": 9096,
            "country": "Bosnia and Herzegovina",
            "population": 3507017,
            "sq_km_area": 51197,
            "life_expectancy": "71.5",
            "elevation_in_meters": 500,
            "continent": "Europe",
            "abbreviation": "BA",
            "location": "Southern Europe",
            "iso": 70,
            "capital_city": "Sarajevo",
            "lat": "43.9159",
            "long": "17.6791",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Botswana": {
        "All": {
            "confirmed": 50800,
            "recovered": 47477,
            "deaths": 774,
            "country": "Botswana",
            "population": 2291661,
            "sq_km_area": 581730,
            "life_expectancy": "39.3",
            "elevation_in_meters": "1,013",
            "continent": "Africa",
            "abbreviation": "BW",
            "location": "Southern Africa",
            "iso": 72,
            "capital_city": "Gaborone",
            "lat": "-22.3285",
            "long": "24.6849",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Brazil": {
        "All": {
            "confirmed": 15812055,
            "recovered": 14045321,
            "deaths": 441691,
            "country": "Brazil",
            "population": 209288278,
            "sq_km_area": 8547403,
            "life_expectancy": "62.9",
            "elevation_in_meters": 320,
            "continent": "South America",
            "abbreviation": "BR",
            "location": "South America",
            "iso": 76,
            "capital_city": "Bras\u00edlia"
        },
        "Acre": {
            "lat": "-9.0238",
            "long": "-70.812",
            "confirmed": 80688,
            "recovered": 73085,
            "deaths": 1631,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Alagoas": {
            "lat": "-9.5713",
            "long": "-36.782",
            "confirmed": 186122,
            "recovered": 177282,
            "deaths": 4538,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Amapa": {
            "lat": "0.902",
            "long": "-52.003",
            "confirmed": 109625,
            "recovered": 82294,
            "deaths": 1635,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Amazonas": {
            "lat": "-3.4168",
            "long": "-65.8561",
            "confirmed": 380416,
            "recovered": 326120,
            "deaths": 12893,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bahia": {
            "lat": "-12.5797",
            "long": "-41.7007",
            "confirmed": 966268,
            "recovered": 928500,
            "deaths": 20131,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ceara": {
            "lat": "-5.4984",
            "long": "-39.3206",
            "confirmed": 752754,
            "recovered": 519432,
            "deaths": 19439,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Distrito Federal": {
            "lat": "-15.7998",
            "long": "-47.8645",
            "confirmed": 395213,
            "recovered": 376920,
            "deaths": 8399,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Espirito Santo": {
            "lat": "-19.1834",
            "long": "-40.3089",
            "confirmed": 463443,
            "recovered": 434940,
            "deaths": 10342,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Goias": {
            "lat": "-15.827",
            "long": "-49.8362",
            "confirmed": 586482,
            "recovered": 556945,
            "deaths": 16301,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Maranhao": {
            "lat": "-4.9609",
            "long": "-45.2744",
            "confirmed": 281250,
            "recovered": 249760,
            "deaths": 7789,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mato Grosso": {
            "lat": "-12.6819",
            "long": "-56.9211",
            "confirmed": 385848,
            "recovered": 366623,
            "deaths": 10373,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mato Grosso do Sul": {
            "lat": "-20.7722",
            "long": "-54.7852",
            "confirmed": 269557,
            "recovered": 249323,
            "deaths": 6310,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Minas Gerais": {
            "lat": "-18.5122",
            "long": "-44.555",
            "confirmed": 1483200,
            "recovered": 1368401,
            "deaths": 37927,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Para": {
            "lat": "-1.9981",
            "long": "-54.9306",
            "confirmed": 503349,
            "recovered": 471494,
            "deaths": 14080,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Paraiba": {
            "lat": "-7.24",
            "long": "-36.782",
            "confirmed": 312185,
            "recovered": 213874,
            "deaths": 7302,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Parana": {
            "lat": "-25.2521",
            "long": "-52.0215",
            "confirmed": 1039201,
            "recovered": 723296,
            "deaths": 25059,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Pernambuco": {
            "lat": "-8.8137",
            "long": "-36.9541",
            "confirmed": 449533,
            "recovered": 381891,
            "deaths": 15127,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Piaui": {
            "lat": "-7.7183",
            "long": "-42.7289",
            "confirmed": 260812,
            "recovered": 254023,
            "deaths": 5677,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rio Grande do Norte": {
            "lat": "-5.4026",
            "long": "-36.9541",
            "confirmed": 253101,
            "recovered": 150649,
            "deaths": 5890,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rio Grande do Sul": {
            "lat": "-30.0346",
            "long": "-51.2177",
            "confirmed": 1043927,
            "recovered": 999737,
            "deaths": 27031,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rio de Janeiro": {
            "lat": "-22.9068",
            "long": "-43.1729",
            "confirmed": 828283,
            "recovered": 766981,
            "deaths": 48662,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rondonia": {
            "lat": "-11.5057",
            "long": "-63.5806",
            "confirmed": 223294,
            "recovered": 211039,
            "deaths": 5545,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Roraima": {
            "lat": "-2.7376",
            "long": "-62.0751",
            "confirmed": 100700,
            "recovered": 94757,
            "deaths": 1584,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Santa Catarina": {
            "lat": "-27.2423",
            "long": "-50.2189",
            "confirmed": 936823,
            "recovered": 901919,
            "deaths": 14621,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sao Paulo": {
            "lat": "-23.5505",
            "long": "-46.6333",
            "confirmed": 3129412,
            "recovered": 2812192,
            "deaths": 105852,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sergipe": {
            "lat": "-10.5741",
            "long": "-37.3857",
            "confirmed": 220619,
            "recovered": 200282,
            "deaths": 4802,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tocantins": {
            "lat": "-10.1753",
            "long": "-48.2982",
            "confirmed": 169950,
            "recovered": 153562,
            "deaths": 2751,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Brunei": {
        "All": {
            "confirmed": 235,
            "recovered": 222,
            "deaths": 3,
            "country": "Brunei",
            "population": 428697,
            "sq_km_area": 5765,
            "life_expectancy": "73.6",
            "elevation_in_meters": 478,
            "continent": "Asia",
            "abbreviation": "BN",
            "location": "Southeast Asia",
            "iso": 96,
            "capital_city": "Bandar Seri Begawan",
            "lat": "4.5353",
            "long": "114.7277",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Bulgaria": {
        "All": {
            "confirmed": 415687,
            "recovered": 369781,
            "deaths": 17416,
            "country": "Bulgaria",
            "population": 7084571,
            "sq_km_area": 110994,
            "life_expectancy": "70.9",
            "elevation_in_meters": 472,
            "continent": "Europe",
            "abbreviation": "BG",
            "location": "Eastern Europe",
            "iso": 100,
            "capital_city": "Sofia",
            "lat": "42.7339",
            "long": "25.4858",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Burkina Faso": {
        "All": {
            "confirmed": 13407,
            "recovered": 13220,
            "deaths": 165,
            "country": "Burkina Faso",
            "population": 19193382,
            "sq_km_area": 274000,
            "life_expectancy": "46.7",
            "elevation_in_meters": 297,
            "continent": "Africa",
            "abbreviation": "BF",
            "location": "Western Africa",
            "iso": 854,
            "capital_city": "Ouagadougou",
            "lat": "12.2383",
            "long": "-1.5616",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Burma": {
        "All": {
            "confirmed": 143183,
            "recovered": 132148,
            "deaths": 3216,
            "lat": "21.9162",
            "long": "95.956",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Burundi": {
        "All": {
            "confirmed": 4382,
            "recovered": 773,
            "deaths": 6,
            "country": "Burundi",
            "population": 10864245,
            "sq_km_area": 27834,
            "life_expectancy": "46.2",
            "elevation_in_meters": "1,504",
            "continent": "Africa",
            "abbreviation": "BI",
            "location": "Eastern Africa",
            "iso": 108,
            "capital_city": "Bujumbura",
            "lat": "-3.3731",
            "long": "29.9189",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cabo Verde": {
        "All": {
            "confirmed": 28699,
            "recovered": 25816,
            "deaths": 253,
            "lat": "16.5388",
            "long": "-23.0418",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cambodia": {
        "All": {
            "confirmed": 23697,
            "recovered": 15700,
            "deaths": 164,
            "country": "Cambodia",
            "population": 16005373,
            "sq_km_area": 181035,
            "life_expectancy": "56.5",
            "elevation_in_meters": 126,
            "continent": "Asia",
            "abbreviation": "KH",
            "location": "Southeast Asia",
            "iso": 116,
            "capital_city": "Phnom Penh",
            "lat": "11.55",
            "long": "104.9167",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cameroon": {
        "All": {
            "confirmed": 77429,
            "recovered": 35261,
            "deaths": 1189,
            "country": "Cameroon",
            "population": 24053727,
            "sq_km_area": 475442,
            "life_expectancy": "54.8",
            "elevation_in_meters": 667,
            "continent": "Africa",
            "abbreviation": "CM",
            "location": "Central Africa",
            "iso": 120,
            "capital_city": "Yaound",
            "lat": "3.848",
            "long": "11.5021",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Canada": {
        "All": {
            "confirmed": 1352959,
            "recovered": 1268466,
            "deaths": 25067,
            "country": "Canada",
            "population": 36624199,
            "sq_km_area": 9970610,
            "life_expectancy": "79.4",
            "elevation_in_meters": 487,
            "continent": "North America",
            "abbreviation": "CA",
            "location": "North America",
            "iso": 124,
            "capital_city": "Ottawa"
        },
        "Alberta": {
            "lat": "53.9333",
            "long": "-116.5765",
            "confirmed": 221467,
            "recovered": 200496,
            "deaths": 2158,
            "updated": "2021/05/20 17:21:11+00"
        },
        "British Columbia": {
            "lat": "53.7267",
            "long": "-127.6476",
            "confirmed": 140596,
            "recovered": 133985,
            "deaths": 1658,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Diamond Princess": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 1,
            "updated": "2020/12/21 13:27:30+00"
        },
        "Grand Princess": {
            "lat": "",
            "long": "",
            "confirmed": 13,
            "recovered": 13,
            "deaths": 0,
            "updated": "2020/12/21 13:27:30+00"
        },
        "Manitoba": {
            "lat": "53.7609",
            "long": "-98.8139",
            "confirmed": 46314,
            "recovered": 40748,
            "deaths": 1016,
            "updated": "2021/05/20 17:21:11+00"
        },
        "New Brunswick": {
            "lat": "46.5653",
            "long": "-66.4619",
            "confirmed": 2091,
            "recovered": 1931,
            "deaths": 43,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Newfoundland and Labrador": {
            "lat": "53.1355",
            "long": "-57.6604",
            "confirmed": 1216,
            "recovered": 1134,
            "deaths": 6,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Northwest Territories": {
            "lat": "64.8255",
            "long": "-124.8457",
            "confirmed": 126,
            "recovered": 109,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nova Scotia": {
            "lat": "44.682",
            "long": "-63.7443",
            "confirmed": 5000,
            "recovered": 3664,
            "deaths": 74,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nunavut": {
            "lat": "70.2998",
            "long": "-83.1076",
            "confirmed": 636,
            "recovered": 577,
            "deaths": 4,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ontario": {
            "lat": "51.2538",
            "long": "-85.3232",
            "confirmed": 524580,
            "recovered": 495468,
            "deaths": 8517,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Prince Edward Island": {
            "lat": "46.5107",
            "long": "-63.4168",
            "confirmed": 199,
            "recovered": 185,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Quebec": {
            "lat": "52.9399",
            "long": "-73.5491",
            "confirmed": 365642,
            "recovered": 347387,
            "deaths": 11066,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Repatriated Travellers": {
            "lat": "",
            "long": "",
            "confirmed": 13,
            "recovered": 13,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saskatchewan": {
            "lat": "52.9399",
            "long": "-106.4509",
            "confirmed": 44982,
            "recovered": 42674,
            "deaths": 522,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yukon": {
            "lat": "64.2823",
            "long": "-135",
            "confirmed": 84,
            "recovered": 82,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Central African Republic": {
        "All": {
            "confirmed": 7010,
            "recovered": 5112,
            "deaths": 96,
            "country": "Central African Republic",
            "population": 4659080,
            "sq_km_area": 622984,
            "life_expectancy": 44,
            "elevation_in_meters": 635,
            "continent": "Africa",
            "abbreviation": "CF",
            "location": "Central Africa",
            "iso": 140,
            "capital_city": "Bangui",
            "lat": "6.6111",
            "long": "20.9394",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Chad": {
        "All": {
            "confirmed": 4914,
            "recovered": 4711,
            "deaths": 173,
            "country": "Chad",
            "population": 14899994,
            "sq_km_area": 1284000,
            "life_expectancy": "50.5",
            "elevation_in_meters": 543,
            "continent": "Africa",
            "abbreviation": "TD",
            "location": "Central Africa",
            "iso": 148,
            "capital_city": "N'Djam",
            "lat": "15.4542",
            "long": "18.7322",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Chile": {
        "All": {
            "confirmed": 1300629,
            "recovered": 1237410,
            "deaths": 27997,
            "country": "Chile",
            "population": 18054726,
            "sq_km_area": 756626,
            "life_expectancy": "75.7",
            "elevation_in_meters": "1,871",
            "continent": "South America",
            "abbreviation": "CL",
            "location": "South America",
            "iso": 152,
            "capital_city": "Santiago de Chile"
        },
        "Antofagasta": {
            "lat": "-23.6509",
            "long": "-70.3975",
            "confirmed": 51392,
            "recovered": 49389,
            "deaths": 1003,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Araucania": {
            "lat": "-38.9489",
            "long": "-72.3311",
            "confirmed": 78407,
            "recovered": 75108,
            "deaths": 979,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Arica y Parinacota": {
            "lat": "-18.594",
            "long": "-69.4785",
            "confirmed": 21703,
            "recovered": 20667,
            "deaths": 416,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Atacama": {
            "lat": "-27.5661",
            "long": "-70.0503",
            "confirmed": 19994,
            "recovered": 19060,
            "deaths": 218,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Aysen": {
            "lat": "-45.9864",
            "long": "-73.7669",
            "confirmed": 5080,
            "recovered": 4713,
            "deaths": 33,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Biobio": {
            "lat": "-37.4464",
            "long": "-72.1416",
            "confirmed": 123813,
            "recovered": 118661,
            "deaths": 1992,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Coquimbo": {
            "lat": "-29.959",
            "long": "-71.3389",
            "confirmed": 35090,
            "recovered": 33415,
            "deaths": 659,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Los Lagos": {
            "lat": "-41.9198",
            "long": "-72.1416",
            "confirmed": 73213,
            "recovered": 70576,
            "deaths": 993,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Los Rios": {
            "lat": "-40.231",
            "long": "-72.3311",
            "confirmed": 36545,
            "recovered": 35145,
            "deaths": 412,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Magallanes": {
            "lat": "-52.368",
            "long": "-70.9863",
            "confirmed": 27009,
            "recovered": 26098,
            "deaths": 391,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Maule": {
            "lat": "-35.5183",
            "long": "-71.6885",
            "confirmed": 79063,
            "recovered": 74905,
            "deaths": 1300,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Metropolitana": {
            "lat": "-33.4376",
            "long": "-70.6505",
            "confirmed": 539242,
            "recovered": 509315,
            "deaths": 14892,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nuble": {
            "lat": "-36.7226",
            "long": "-71.7622",
            "confirmed": 28349,
            "recovered": 27905,
            "deaths": 478,
            "updated": "2021/05/20 17:21:11+00"
        },
        "OHiggins": {
            "lat": "-34.5755",
            "long": "-71.0022",
            "confirmed": 53111,
            "recovered": 50096,
            "deaths": 1118,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tarapaca": {
            "lat": "-19.9232",
            "long": "-69.5132",
            "confirmed": 35751,
            "recovered": 34469,
            "deaths": 648,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 54,
            "recovered": 54,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Valparaiso": {
            "lat": "-33.0472",
            "long": "-71.6127",
            "confirmed": 92813,
            "recovered": 87834,
            "deaths": 2465,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "China": {
        "All": {
            "confirmed": 102798,
            "recovered": 97578,
            "deaths": 4846,
            "country": "China",
            "population": 1409517397,
            "sq_km_area": 9572900,
            "life_expectancy": "71.4",
            "elevation_in_meters": "1,840",
            "continent": "Asia",
            "abbreviation": "CN",
            "location": "Eastern Asia",
            "iso": 156,
            "capital_city": "Peking"
        },
        "Anhui": {
            "lat": "31.8257",
            "long": "117.2264",
            "confirmed": 1001,
            "recovered": 989,
            "deaths": 6,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Beijing": {
            "lat": "40.1824",
            "long": "116.4142",
            "confirmed": 1057,
            "recovered": 1046,
            "deaths": 9,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chongqing": {
            "lat": "30.0572",
            "long": "107.874",
            "confirmed": 598,
            "recovered": 586,
            "deaths": 6,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Fujian": {
            "lat": "26.0789",
            "long": "117.9874",
            "confirmed": 599,
            "recovered": 581,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gansu": {
            "lat": "35.7518",
            "long": "104.2861",
            "confirmed": 193,
            "recovered": 191,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guangdong": {
            "lat": "23.3417",
            "long": "113.4244",
            "confirmed": 2399,
            "recovered": 2354,
            "deaths": 8,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guangxi": {
            "lat": "23.8298",
            "long": "108.7881",
            "confirmed": 275,
            "recovered": 267,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guizhou": {
            "lat": "26.8154",
            "long": "106.8748",
            "confirmed": 147,
            "recovered": 145,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hainan": {
            "lat": "19.1959",
            "long": "109.7453",
            "confirmed": 188,
            "recovered": 176,
            "deaths": 6,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hebei": {
            "lat": "37.8957",
            "long": "114.9042",
            "confirmed": 1317,
            "recovered": 1310,
            "deaths": 7,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Heilongjiang": {
            "lat": "47.862",
            "long": "127.7615",
            "confirmed": 1611,
            "recovered": 1597,
            "deaths": 13,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Henan": {
            "lat": "33.882",
            "long": "113.614",
            "confirmed": 1313,
            "recovered": 1289,
            "deaths": 22,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hong Kong": {
            "lat": "22.3",
            "long": "114.2",
            "confirmed": 11828,
            "recovered": 11542,
            "deaths": 210,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hubei": {
            "lat": "30.9756",
            "long": "112.2707",
            "confirmed": 68159,
            "recovered": 63644,
            "deaths": 4512,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hunan": {
            "lat": "27.6104",
            "long": "111.7088",
            "confirmed": 1048,
            "recovered": 1038,
            "deaths": 4,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Inner Mongolia": {
            "lat": "44.0935",
            "long": "113.9448",
            "confirmed": 384,
            "recovered": 380,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jiangsu": {
            "lat": "32.9711",
            "long": "119.455",
            "confirmed": 722,
            "recovered": 713,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jiangxi": {
            "lat": "27.614",
            "long": "115.7221",
            "confirmed": 937,
            "recovered": 936,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jilin": {
            "lat": "43.6661",
            "long": "126.1923",
            "confirmed": 573,
            "recovered": 570,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Liaoning": {
            "lat": "41.2956",
            "long": "122.6085",
            "confirmed": 423,
            "recovered": 406,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Macau": {
            "lat": "22.1667",
            "long": "113.55",
            "confirmed": 50,
            "recovered": 49,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ningxia": {
            "lat": "37.2692",
            "long": "106.1655",
            "confirmed": 76,
            "recovered": 75,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Qinghai": {
            "lat": "35.7452",
            "long": "95.9956",
            "confirmed": 18,
            "recovered": 18,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shaanxi": {
            "lat": "35.1917",
            "long": "108.8701",
            "confirmed": 609,
            "recovered": 590,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shandong": {
            "lat": "36.3427",
            "long": "118.1498",
            "confirmed": 883,
            "recovered": 872,
            "deaths": 7,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shanghai": {
            "lat": "31.202",
            "long": "121.4491",
            "confirmed": 2052,
            "recovered": 1985,
            "deaths": 7,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shanxi": {
            "lat": "37.5777",
            "long": "112.2922",
            "confirmed": 251,
            "recovered": 246,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sichuan": {
            "lat": "30.6171",
            "long": "102.7103",
            "confirmed": 1005,
            "recovered": 976,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tianjin": {
            "lat": "39.3054",
            "long": "117.323",
            "confirmed": 389,
            "recovered": 379,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tibet": {
            "lat": "31.6927",
            "long": "88.0924",
            "confirmed": 1,
            "recovered": 1,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 8,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Xinjiang": {
            "lat": "41.1129",
            "long": "85.2401",
            "confirmed": 980,
            "recovered": 977,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yunnan": {
            "lat": "24.974",
            "long": "101.487",
            "confirmed": 351,
            "recovered": 318,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Zhejiang": {
            "lat": "29.1832",
            "long": "120.0934",
            "confirmed": 1361,
            "recovered": 1324,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Colombia": {
        "All": {
            "confirmed": 3161126,
            "recovered": 2961313,
            "deaths": 82743,
            "country": "Colombia",
            "population": 49065615,
            "sq_km_area": 1138914,
            "life_expectancy": "70.3",
            "elevation_in_meters": 593,
            "continent": "South America",
            "abbreviation": "CO",
            "location": "South America",
            "iso": 170,
            "capital_city": "Santaf"
        },
        "Amazonas": {
            "lat": "-1.4429",
            "long": "-71.5724",
            "confirmed": 6300,
            "recovered": 6008,
            "deaths": 238,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Antioquia": {
            "lat": "7.1986",
            "long": "-75.3412",
            "confirmed": 510159,
            "recovered": 482203,
            "deaths": 11236,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Arauca": {
            "lat": "7.0762",
            "long": "-70.7105",
            "confirmed": 7118,
            "recovered": 6644,
            "deaths": 206,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Atlantico": {
            "lat": "10.6966",
            "long": "-74.8741",
            "confirmed": 261851,
            "recovered": 248026,
            "deaths": 8086,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bolivar": {
            "lat": "8.6704",
            "long": "-74.03",
            "confirmed": 93721,
            "recovered": 89038,
            "deaths": 1803,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Boyaca": {
            "lat": "5.4545",
            "long": "-73.362",
            "confirmed": 60833,
            "recovered": 56759,
            "deaths": 1412,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Caldas": {
            "lat": "5.2983",
            "long": "-75.2479",
            "confirmed": 66881,
            "recovered": 63582,
            "deaths": 1384,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Capital District": {
            "lat": "4.711",
            "long": "-74.0721",
            "confirmed": 879179,
            "recovered": 811968,
            "deaths": 17535,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Caqueta": {
            "lat": "0.8699",
            "long": "-73.8419",
            "confirmed": 17825,
            "recovered": 16941,
            "deaths": 668,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Casanare": {
            "lat": "5.7589",
            "long": "-71.5724",
            "confirmed": 18053,
            "recovered": 16624,
            "deaths": 414,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cauca": {
            "lat": "2.705",
            "long": "-76.826",
            "confirmed": 32328,
            "recovered": 30650,
            "deaths": 860,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cesar": {
            "lat": "9.3373",
            "long": "-73.6536",
            "confirmed": 62522,
            "recovered": 59096,
            "deaths": 1682,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Choco": {
            "lat": "5.2528",
            "long": "-76.826",
            "confirmed": 9892,
            "recovered": 9162,
            "deaths": 259,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cordoba": {
            "lat": "8.0493",
            "long": "-75.574",
            "confirmed": 59363,
            "recovered": 54960,
            "deaths": 2391,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cundinamarca": {
            "lat": "5.026",
            "long": "-74.03",
            "confirmed": 146438,
            "recovered": 136338,
            "deaths": 3971,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guainia": {
            "lat": "2.5854",
            "long": "-68.5247",
            "confirmed": 1572,
            "recovered": 1484,
            "deaths": 22,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guaviare": {
            "lat": "1.0654",
            "long": "-73.2603",
            "confirmed": 2656,
            "recovered": 2472,
            "deaths": 40,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Huila": {
            "lat": "2.5359",
            "long": "-75.5277",
            "confirmed": 56255,
            "recovered": 53027,
            "deaths": 1952,
            "updated": "2021/05/20 17:21:11+00"
        },
        "La Guajira": {
            "lat": "11.3548",
            "long": "-72.5205",
            "confirmed": 34001,
            "recovered": 32127,
            "deaths": 1086,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Magdalena": {
            "lat": "10.4113",
            "long": "-74.4057",
            "confirmed": 66423,
            "recovered": 61827,
            "deaths": 2529,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Meta": {
            "lat": "3.272",
            "long": "-73.0877",
            "confirmed": 49487,
            "recovered": 47566,
            "deaths": 1170,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Narino": {
            "lat": "1.2892",
            "long": "-77.3579",
            "confirmed": 58764,
            "recovered": 54847,
            "deaths": 1909,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Norte de Santander": {
            "lat": "7.9463",
            "long": "-72.8988",
            "confirmed": 58096,
            "recovered": 53745,
            "deaths": 2952,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Putumayo": {
            "lat": "0.436",
            "long": "-75.5277",
            "confirmed": 11171,
            "recovered": 10381,
            "deaths": 436,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Quindio": {
            "lat": "4.461",
            "long": "-75.6674",
            "confirmed": 42167,
            "recovered": 40072,
            "deaths": 1262,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Risaralda": {
            "lat": "5.3158",
            "long": "-75.9928",
            "confirmed": 60287,
            "recovered": 57573,
            "deaths": 1523,
            "updated": "2021/05/20 17:21:11+00"
        },
        "San Andres y Providencia": {
            "lat": "12.5567",
            "long": "-81.7185",
            "confirmed": 4694,
            "recovered": 4367,
            "deaths": 74,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Santander": {
            "lat": "6.6437",
            "long": "-73.6536",
            "confirmed": 120312,
            "recovered": 110557,
            "deaths": 4120,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sucre": {
            "lat": "8.814",
            "long": "-74.7233",
            "confirmed": 32271,
            "recovered": 29982,
            "deaths": 1092,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tolima": {
            "lat": "4.0925",
            "long": "-75.1545",
            "confirmed": 72827,
            "recovered": 69160,
            "deaths": 2352,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Valle del Cauca": {
            "lat": "3.8009",
            "long": "-76.6413",
            "confirmed": 254620,
            "recovered": 241162,
            "deaths": 8040,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Vaupes": {
            "lat": "0.8554",
            "long": "-70.812",
            "confirmed": 1415,
            "recovered": 1366,
            "deaths": 14,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Vichada": {
            "lat": "4.4234",
            "long": "-69.2878",
            "confirmed": 1645,
            "recovered": 1599,
            "deaths": 25,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Comoros": {
        "All": {
            "confirmed": 3864,
            "recovered": 3708,
            "deaths": 146,
            "country": "Comoros",
            "population": 813912,
            "sq_km_area": 1862,
            "life_expectancy": 60,
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "KM",
            "location": "Eastern Africa",
            "iso": 174,
            "capital_city": "Moroni",
            "lat": "-11.6455",
            "long": "43.3333",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Congo (Brazzaville)": {
        "All": {
            "confirmed": 11476,
            "recovered": 8208,
            "deaths": 150,
            "lat": "-0.228",
            "long": "15.8277",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Congo (Kinshasa)": {
        "All": {
            "confirmed": 30757,
            "recovered": 27603,
            "deaths": 779,
            "lat": "-4.0383",
            "long": "21.7587",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Costa Rica": {
        "All": {
            "confirmed": 293820,
            "recovered": 225950,
            "deaths": 3696,
            "country": "Costa Rica",
            "population": 4905769,
            "sq_km_area": 51100,
            "life_expectancy": "75.8",
            "elevation_in_meters": 746,
            "continent": "North America",
            "abbreviation": "CR",
            "location": "Central America",
            "iso": 188,
            "capital_city": "San Jos",
            "lat": "9.7489",
            "long": "-83.7534",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cote d'Ivoire": {
        "All": {
            "confirmed": 46777,
            "recovered": 46217,
            "deaths": 298,
            "country": "Cote d'Ivoire",
            "population": 24294750,
            "sq_km_area": 322463,
            "life_expectancy": "45.2",
            "elevation_in_meters": 250,
            "continent": "Africa",
            "abbreviation": "CI",
            "location": "Western Africa",
            "iso": 384,
            "capital_city": "Yamoussoukro",
            "lat": "7.54",
            "long": "-5.5471",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Croatia": {
        "All": {
            "confirmed": 352692,
            "recovered": 339780,
            "deaths": 7828,
            "country": "Croatia",
            "population": 4189353,
            "sq_km_area": 56538,
            "life_expectancy": "73.7",
            "elevation_in_meters": 331,
            "continent": "Europe",
            "abbreviation": "HR",
            "location": "Southern Europe",
            "iso": 191,
            "capital_city": "Zagreb",
            "lat": "45.1",
            "long": "15.2",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cuba": {
        "All": {
            "confirmed": 129346,
            "recovered": 121594,
            "deaths": 840,
            "country": "Cuba",
            "population": 11484636,
            "sq_km_area": 110861,
            "life_expectancy": "76.2",
            "elevation_in_meters": 108,
            "continent": "North America",
            "abbreviation": "CU",
            "location": "Caribbean",
            "iso": 192,
            "capital_city": "La Habana",
            "lat": "21.521757",
            "long": "-77.781167",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Cyprus": {
        "All": {
            "confirmed": 71533,
            "recovered": 39061,
            "deaths": 348,
            "country": "Cyprus",
            "population": 1179551,
            "sq_km_area": 9251,
            "life_expectancy": "76.7",
            "elevation_in_meters": 91,
            "continent": "Asia",
            "abbreviation": "CY",
            "location": "Middle East",
            "iso": 196,
            "capital_city": "Nicosia",
            "lat": "35.1264",
            "long": "33.4299",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Czechia": {
        "All": {
            "confirmed": 1656202,
            "recovered": 1602974,
            "deaths": 29967,
            "country": "Czechia",
            "population": 10618303,
            "sq_km_area": 78866,
            "life_expectancy": "74.5",
            "elevation_in_meters": 433,
            "continent": "Europe",
            "abbreviation": "CZ",
            "location": "Eastern Europe",
            "iso": 203,
            "capital_city": "Praha",
            "lat": "49.8175",
            "long": "15.473",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Denmark": {
        "All": {
            "confirmed": 271261,
            "recovered": 255438,
            "deaths": 2507,
            "country": "Denmark",
            "population": 5733551,
            "sq_km_area": 43094,
            "life_expectancy": "76.5",
            "elevation_in_meters": 34,
            "continent": "Europe",
            "abbreviation": "DK",
            "location": "Nordic Countries",
            "iso": 208,
            "capital_city": "Copenhagen",
            "lat": "56.2639",
            "long": "9.5018",
            "updated": "2021/05/20 17:21:11+00"
        },
        "Faroe Islands": {
            "lat": "61.8926",
            "long": "-6.9118",
            "confirmed": 670,
            "recovered": 667,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Greenland": {
            "lat": "71.7069",
            "long": "-42.6043",
            "confirmed": 34,
            "recovered": 32,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Diamond Princess": {
        "All": {
            "confirmed": 712,
            "recovered": 699,
            "deaths": 13,
            "lat": "",
            "long": "",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Djibouti": {
        "All": {
            "confirmed": 11485,
            "recovered": 11305,
            "deaths": 152,
            "country": "Djibouti",
            "population": 956985,
            "sq_km_area": 23200,
            "life_expectancy": "50.8",
            "elevation_in_meters": 430,
            "continent": "Africa",
            "abbreviation": "DJ",
            "location": "Eastern Africa",
            "iso": 262,
            "capital_city": "Djibouti",
            "lat": "11.8251",
            "long": "42.5903",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Dominica": {
        "All": {
            "confirmed": 184,
            "recovered": 176,
            "deaths": 0,
            "country": "Dominica",
            "population": 73925,
            "sq_km_area": 751,
            "life_expectancy": "73.4",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "DM",
            "location": "Caribbean",
            "iso": 212,
            "capital_city": "Roseau",
            "lat": "15.415",
            "long": "-61.371",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Dominican Republic": {
        "All": {
            "confirmed": 280994,
            "recovered": 235284,
            "deaths": 3600,
            "country": "Dominican Republic",
            "population": 10766998,
            "sq_km_area": 48511,
            "life_expectancy": "73.2",
            "elevation_in_meters": 424,
            "continent": "North America",
            "abbreviation": "DO",
            "location": "Caribbean",
            "iso": 214,
            "capital_city": "Santo Domingo de Guzm",
            "lat": "18.7357",
            "long": "-70.1627",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Ecuador": {
        "All": {
            "confirmed": 412907,
            "recovered": 354499,
            "deaths": 19979,
            "country": "Ecuador",
            "population": 16624858,
            "sq_km_area": 283561,
            "life_expectancy": "71.1",
            "elevation_in_meters": "1,117",
            "continent": "South America",
            "abbreviation": "EC",
            "location": "South America",
            "iso": 218,
            "capital_city": "Quito",
            "lat": "-1.8312",
            "long": "-78.1834",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Egypt": {
        "All": {
            "confirmed": 249238,
            "recovered": 183696,
            "deaths": 14498,
            "country": "Egypt",
            "population": 97553151,
            "sq_km_area": 1001449,
            "life_expectancy": "63.3",
            "elevation_in_meters": 321,
            "continent": "Africa",
            "abbreviation": "EG",
            "location": "Northern Africa",
            "iso": 818,
            "capital_city": "Cairo",
            "lat": "26.820553",
            "long": "30.802498",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "El Salvador": {
        "All": {
            "confirmed": 71479,
            "recovered": 66903,
            "deaths": 2202,
            "country": "El Salvador",
            "population": 6377853,
            "sq_km_area": 21041,
            "life_expectancy": "69.7",
            "elevation_in_meters": 442,
            "continent": "North America",
            "abbreviation": "SV",
            "location": "Central America",
            "iso": 222,
            "capital_city": "San Salvador",
            "lat": "13.7942",
            "long": "-88.8965",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Equatorial Guinea": {
        "All": {
            "confirmed": 8436,
            "recovered": 7801,
            "deaths": 113,
            "country": "Equatorial Guinea",
            "population": 1267689,
            "sq_km_area": 28051,
            "life_expectancy": "53.6",
            "elevation_in_meters": 577,
            "continent": "Africa",
            "abbreviation": "GQ",
            "location": "Central Africa",
            "iso": 226,
            "capital_city": "Malabo",
            "lat": "1.6508",
            "long": "10.2679",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Eritrea": {
        "All": {
            "confirmed": 3906,
            "recovered": 3700,
            "deaths": 14,
            "country": "Eritrea",
            "population": 5068831,
            "sq_km_area": 117600,
            "life_expectancy": "55.8",
            "elevation_in_meters": 853,
            "continent": "Africa",
            "abbreviation": "ER",
            "location": "Eastern Africa",
            "iso": 232,
            "capital_city": "Asmara",
            "lat": "15.1794",
            "long": "39.7823",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Estonia": {
        "All": {
            "confirmed": 128084,
            "recovered": 119799,
            "deaths": 1235,
            "country": "Estonia",
            "population": 1309632,
            "sq_km_area": 45227,
            "life_expectancy": "69.5",
            "elevation_in_meters": 61,
            "continent": "Europe",
            "abbreviation": "EE",
            "location": "Baltic Countries",
            "iso": 233,
            "capital_city": "Tallinn",
            "lat": "58.5953",
            "long": "25.0136",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Eswatini": {
        "All": {
            "confirmed": 18530,
            "recovered": 17808,
            "deaths": 672,
            "lat": "-26.5225",
            "long": "31.4659",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Ethiopia": {
        "All": {
            "confirmed": 267597,
            "recovered": 222560,
            "deaths": 4038,
            "country": "Ethiopia",
            "population": 104957438,
            "sq_km_area": 1104300,
            "life_expectancy": "45.2",
            "elevation_in_meters": "1,330",
            "continent": "Africa",
            "abbreviation": "ET",
            "location": "Eastern Africa",
            "iso": 231,
            "capital_city": "Addis Abeba",
            "lat": "9.145",
            "long": "40.4897",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Fiji": {
        "All": {
            "confirmed": 190,
            "recovered": 129,
            "deaths": 4,
            "country": "Fiji",
            "population": 905502,
            "sq_km_area": 18274,
            "life_expectancy": "67.9",
            "elevation_in_meters": null,
            "continent": "Oceania",
            "abbreviation": "FJ",
            "location": "Melanesia",
            "iso": null,
            "capital_city": "Suva",
            "lat": "-17.7134",
            "long": "178.065",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Finland": {
        "All": {
            "confirmed": 90946,
            "recovered": 46000,
            "deaths": 929,
            "country": "Finland",
            "population": 5523231,
            "sq_km_area": 338145,
            "life_expectancy": "77.4",
            "elevation_in_meters": 164,
            "continent": "Europe",
            "abbreviation": "FI",
            "location": "Nordic Countries",
            "iso": 246,
            "capital_city": "Helsinki [Helsingfors]",
            "lat": "61.92411",
            "long": "25.748151",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "France": {
        "All": {
            "confirmed": 5979093,
            "recovered": 382501,
            "deaths": 108343,
            "country": "France",
            "population": 64979548,
            "sq_km_area": 551500,
            "life_expectancy": "78.8",
            "elevation_in_meters": 375,
            "continent": "Europe",
            "abbreviation": "FR",
            "location": "Western Europe",
            "iso": 250,
            "capital_city": "Paris",
            "lat": "46.2276",
            "long": "2.2137",
            "updated": "2021/05/20 17:21:11+00"
        },
        "French Guiana": {
            "lat": "4",
            "long": "-53",
            "confirmed": 22115,
            "recovered": 9995,
            "deaths": 110,
            "updated": "2021/05/20 17:21:11+00"
        },
        "French Polynesia": {
            "lat": "-17.6797",
            "long": "-149.4068",
            "confirmed": 18835,
            "recovered": 18661,
            "deaths": 141,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guadeloupe": {
            "lat": "16.265",
            "long": "-61.551",
            "confirmed": 16079,
            "recovered": 2250,
            "deaths": 245,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Martinique": {
            "lat": "14.6415",
            "long": "-61.0242",
            "confirmed": 11669,
            "recovered": 98,
            "deaths": 90,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mayotte": {
            "lat": "-12.8275",
            "long": "45.166244",
            "confirmed": 20176,
            "recovered": 2964,
            "deaths": 171,
            "updated": "2021/05/20 17:21:11+00"
        },
        "New Caledonia": {
            "lat": "-20.904305",
            "long": "165.618042",
            "confirmed": 125,
            "recovered": 58,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Reunion": {
            "lat": "-21.1151",
            "long": "55.5364",
            "confirmed": 23566,
            "recovered": 21709,
            "deaths": 176,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saint Barthelemy": {
            "lat": "17.9",
            "long": "-62.8333",
            "confirmed": 1005,
            "recovered": 462,
            "deaths": 1,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saint Pierre and Miquelon": {
            "lat": "46.8852",
            "long": "-56.3159",
            "confirmed": 25,
            "recovered": 25,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "St Martin": {
            "lat": "18.0708",
            "long": "-63.0501",
            "confirmed": 1915,
            "recovered": 1399,
            "deaths": 12,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Wallis and Futuna": {
            "lat": "-14.2938",
            "long": "-178.1165",
            "confirmed": 445,
            "recovered": 436,
            "deaths": 7,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Gabon": {
        "All": {
            "confirmed": 24039,
            "recovered": 20759,
            "deaths": 147,
            "country": "Gabon",
            "population": 2025137,
            "sq_km_area": 267668,
            "life_expectancy": "50.1",
            "elevation_in_meters": 377,
            "continent": "Africa",
            "abbreviation": "GA",
            "location": "Central Africa",
            "iso": 266,
            "capital_city": "Libreville",
            "lat": "-0.8037",
            "long": "11.6094",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Gambia": {
        "All": {
            "confirmed": 5966,
            "recovered": 5717,
            "deaths": 175,
            "country": "Gambia",
            "population": 2100568,
            "sq_km_area": 11295,
            "life_expectancy": "53.2",
            "elevation_in_meters": 34,
            "continent": "Africa",
            "abbreviation": "GM",
            "location": "Western Africa",
            "iso": 270,
            "capital_city": "Banjul",
            "lat": "13.4432",
            "long": "-15.3101",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Georgia": {
        "All": {
            "confirmed": 334705,
            "recovered": 315254,
            "deaths": 4540,
            "country": "Georgia",
            "population": 3912061,
            "sq_km_area": 69700,
            "life_expectancy": "64.5",
            "elevation_in_meters": "1,432",
            "continent": "Asia",
            "abbreviation": "GE",
            "location": "Middle East",
            "iso": 268,
            "capital_city": "Tbilisi",
            "lat": "42.3154",
            "long": "43.3569",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Germany": {
        "All": {
            "confirmed": 3637296,
            "recovered": 3360400,
            "deaths": 86968,
            "country": "Germany",
            "population": 82114224,
            "sq_km_area": 357022,
            "life_expectancy": "77.4",
            "elevation_in_meters": 263,
            "continent": "Europe",
            "abbreviation": "DE",
            "location": "Western Europe",
            "iso": 276,
            "capital_city": "Berlin"
        },
        "Baden-Wurttemberg": {
            "lat": "48.6616",
            "long": "9.3501",
            "confirmed": 483234,
            "recovered": 446100,
            "deaths": 9780,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bayern": {
            "lat": "48.7904",
            "long": "11.4979",
            "confirmed": 630603,
            "recovered": 588700,
            "deaths": 14703,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Berlin": {
            "lat": "52.52",
            "long": "13.405",
            "confirmed": 176511,
            "recovered": 168000,
            "deaths": 3416,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Brandenburg": {
            "lat": "52.4125",
            "long": "12.5316",
            "confirmed": 107234,
            "recovered": 99300,
            "deaths": 3690,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bremen": {
            "lat": "53.0793",
            "long": "8.8017",
            "confirmed": 26774,
            "recovered": 25710,
            "deaths": 470,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hamburg": {
            "lat": "53.5511",
            "long": "9.9937",
            "confirmed": 75483,
            "recovered": 69500,
            "deaths": 1541,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hessen": {
            "lat": "50.6521",
            "long": "9.1624",
            "confirmed": 281797,
            "recovered": 258300,
            "deaths": 7189,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mecklenburg-Vorpommern": {
            "lat": "53.6127",
            "long": "12.4296",
            "confirmed": 43535,
            "recovered": 40500,
            "deaths": 1103,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Niedersachsen": {
            "lat": "52.6367",
            "long": "9.8451",
            "confirmed": 254251,
            "recovered": 239700,
            "deaths": 5529,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nordrhein-Westfalen": {
            "lat": "51.4332",
            "long": "7.6616",
            "confirmed": 792263,
            "recovered": 731900,
            "deaths": 16408,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rheinland-Pfalz": {
            "lat": "50.1183",
            "long": "7.309",
            "confirmed": 150592,
            "recovered": 139600,
            "deaths": 3678,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saarland": {
            "lat": "49.3964",
            "long": "7.023",
            "confirmed": 40017,
            "recovered": 37500,
            "deaths": 988,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sachsen": {
            "lat": "51.1045",
            "long": "13.2017",
            "confirmed": 280894,
            "recovered": 252900,
            "deaths": 9507,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sachsen-Anhalt": {
            "lat": "51.9503",
            "long": "11.6923",
            "confirmed": 97401,
            "recovered": 88990,
            "deaths": 3275,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Schleswig-Holstein": {
            "lat": "54.2194",
            "long": "9.6961",
            "confirmed": 62122,
            "recovered": 58200,
            "deaths": 1571,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Thuringen": {
            "lat": "51.011",
            "long": "10.8453",
            "confirmed": 125681,
            "recovered": 115500,
            "deaths": 4120,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 8904,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Ghana": {
        "All": {
            "confirmed": 93456,
            "recovered": 91348,
            "deaths": 783,
            "country": "Ghana",
            "population": 28833629,
            "sq_km_area": 238533,
            "life_expectancy": "57.4",
            "elevation_in_meters": 190,
            "continent": "Africa",
            "abbreviation": "GH",
            "location": "Western Africa",
            "iso": 288,
            "capital_city": "Accra",
            "lat": "7.9465",
            "long": "-1.0232",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Greece": {
        "All": {
            "confirmed": 385444,
            "recovered": 93764,
            "deaths": 11641,
            "country": "Greece",
            "population": 11159773,
            "sq_km_area": 131626,
            "life_expectancy": "78.4",
            "elevation_in_meters": 498,
            "continent": "Europe",
            "abbreviation": "GR",
            "location": "Southern Europe",
            "iso": 300,
            "capital_city": "Athenai",
            "lat": "39.0742",
            "long": "21.8243",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Grenada": {
        "All": {
            "confirmed": 161,
            "recovered": 160,
            "deaths": 1,
            "country": "Grenada",
            "population": 107825,
            "sq_km_area": 344,
            "life_expectancy": "64.5",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "GD",
            "location": "Caribbean",
            "iso": 308,
            "capital_city": "Saint George's",
            "lat": "12.1165",
            "long": "-61.679",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Guatemala": {
        "All": {
            "confirmed": 243833,
            "recovered": 224185,
            "deaths": 7928,
            "country": "Guatemala",
            "population": 16913503,
            "sq_km_area": 108889,
            "life_expectancy": "66.2",
            "elevation_in_meters": 759,
            "continent": "North America",
            "abbreviation": "GT",
            "location": "Central America",
            "iso": 320,
            "capital_city": "Ciudad de Guatemala",
            "lat": "15.7835",
            "long": "-90.2308",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Guinea": {
        "All": {
            "confirmed": 22879,
            "recovered": 20547,
            "deaths": 153,
            "country": "Guinea",
            "population": 12717176,
            "sq_km_area": 245857,
            "life_expectancy": "45.6",
            "elevation_in_meters": 472,
            "continent": "Africa",
            "abbreviation": "GN",
            "location": "Western Africa",
            "iso": 324,
            "capital_city": "Conakry",
            "lat": "9.9456",
            "long": "-9.6966",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Guinea-Bissau": {
        "All": {
            "confirmed": 3746,
            "recovered": 3438,
            "deaths": 68,
            "country": "Guinea-Bissau",
            "population": 1861283,
            "sq_km_area": 36125,
            "life_expectancy": 49,
            "elevation_in_meters": 70,
            "continent": "Africa",
            "abbreviation": "GW",
            "location": "Western Africa",
            "iso": 624,
            "capital_city": "Bissau",
            "lat": "11.8037",
            "long": "-15.1804",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Guyana": {
        "All": {
            "confirmed": 15607,
            "recovered": 13359,
            "deaths": 349,
            "country": "Guyana",
            "population": 777859,
            "sq_km_area": 214969,
            "life_expectancy": 64,
            "elevation_in_meters": 207,
            "continent": "South America",
            "abbreviation": "GY",
            "location": "South America",
            "iso": 328,
            "capital_city": "Georgetown",
            "lat": "4.860416",
            "long": "-58.93018",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Haiti": {
        "All": {
            "confirmed": 13598,
            "recovered": 12463,
            "deaths": 276,
            "country": "Haiti",
            "population": 10981229,
            "sq_km_area": 27750,
            "life_expectancy": "49.2",
            "elevation_in_meters": 470,
            "continent": "North America",
            "abbreviation": "HT",
            "location": "Caribbean",
            "iso": 332,
            "capital_city": "Port-au-Prince",
            "lat": "18.9712",
            "long": "-72.2852",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Holy See": {
        "All": {
            "confirmed": 27,
            "recovered": 27,
            "deaths": 0,
            "country": "Holy See",
            "population": 1000,
            "sq_km_area": 0.4,
            "life_expectancy": null,
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "VA",
            "location": "Southern Europe",
            "iso": null,
            "capital_city": "Citt",
            "lat": "41.9029",
            "long": "12.4534",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Honduras": {
        "All": {
            "confirmed": 229211,
            "recovered": 83123,
            "deaths": 6051,
            "country": "Honduras",
            "population": 9265067,
            "sq_km_area": 112088,
            "life_expectancy": "69.9",
            "elevation_in_meters": 684,
            "continent": "North America",
            "abbreviation": "HN",
            "location": "Central America",
            "iso": 340,
            "capital_city": "Tegucigalpa",
            "lat": "15.2",
            "long": "-86.2419",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Hungary": {
        "All": {
            "confirmed": 800368,
            "recovered": 655959,
            "deaths": 29380,
            "country": "Hungary",
            "population": 9721559,
            "sq_km_area": 93030,
            "life_expectancy": "71.4",
            "elevation_in_meters": 143,
            "continent": "Europe",
            "abbreviation": "HU",
            "location": "Eastern Europe",
            "iso": 348,
            "capital_city": "Budapest",
            "lat": "47.1625",
            "long": "19.5033",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Iceland": {
        "All": {
            "confirmed": 6555,
            "recovered": 6476,
            "deaths": 29,
            "country": "Iceland",
            "population": 335025,
            "sq_km_area": 103000,
            "life_expectancy": "79.4",
            "elevation_in_meters": 557,
            "continent": "Europe",
            "abbreviation": "IS",
            "location": "Nordic Countries",
            "iso": 352,
            "capital_city": "Reykjav",
            "lat": "64.9631",
            "long": "-19.0208",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "India": {
        "All": {
            "confirmed": 25772440,
            "recovered": 22355440,
            "deaths": 287122,
            "country": "India",
            "population": 1339180127,
            "sq_km_area": 3287263,
            "life_expectancy": "62.5",
            "elevation_in_meters": 160,
            "continent": "Asia",
            "abbreviation": "IN",
            "location": "Southern and Central Asia",
            "iso": 356,
            "capital_city": "New Delhi"
        },
        "Andaman and Nicobar Islands": {
            "lat": "11.225999",
            "long": "92.968178",
            "confirmed": 6709,
            "recovered": 6385,
            "deaths": 95,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Andhra Pradesh": {
            "lat": "15.9129",
            "long": "79.74",
            "confirmed": 1498532,
            "recovered": 1279110,
            "deaths": 9686,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Arunachal Pradesh": {
            "lat": "27.768456",
            "long": "96.384277",
            "confirmed": 22799,
            "recovered": 20125,
            "deaths": 89,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Assam": {
            "lat": "26.357149",
            "long": "92.830441",
            "confirmed": 347001,
            "recovered": 294831,
            "deaths": 2433,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bihar": {
            "lat": "25.679658",
            "long": "85.60484",
            "confirmed": 670174,
            "recovered": 607420,
            "deaths": 4143,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chandigarh": {
            "lat": "30.733839",
            "long": "76.768278",
            "confirmed": 56927,
            "recovered": 49701,
            "deaths": 656,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chhattisgarh": {
            "lat": "21.264705",
            "long": "82.035366",
            "confirmed": 931211,
            "recovered": 833161,
            "deaths": 12182,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Dadra and Nagar Haveli and Daman and Diu": {
            "lat": "20.194742",
            "long": "73.080901",
            "confirmed": 9748,
            "recovered": 9088,
            "deaths": 4,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Delhi": {
            "lat": "28.646519",
            "long": "77.10898",
            "confirmed": 1406719,
            "recovered": 1339326,
            "deaths": 22346,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Goa": {
            "lat": "15.359682",
            "long": "74.057396",
            "confirmed": 139985,
            "recovered": 114793,
            "deaths": 2228,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gujarat": {
            "lat": "22.694884",
            "long": "71.590923",
            "confirmed": 771447,
            "recovered": 669490,
            "deaths": 9340,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Haryana": {
            "lat": "29.20004",
            "long": "76.332824",
            "confirmed": 716507,
            "recovered": 638673,
            "deaths": 7076,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Himachal Pradesh": {
            "lat": "31.927213",
            "long": "77.233081",
            "confirmed": 170074,
            "recovered": 132421,
            "deaths": 2529,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jammu and Kashmir": {
            "lat": "33.75943",
            "long": "76.612638",
            "confirmed": 255888,
            "recovered": 202039,
            "deaths": 3355,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jharkhand": {
            "lat": "23.654536",
            "long": "85.557631",
            "confirmed": 322828,
            "recovered": 289333,
            "deaths": 4654,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Karnataka": {
            "lat": "14.70518",
            "long": "76.166436",
            "confirmed": 2306655,
            "recovered": 1724438,
            "deaths": 23306,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kerala": {
            "lat": "10.450898",
            "long": "76.405749",
            "confirmed": 2233468,
            "recovered": 1894518,
            "deaths": 6724,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ladakh": {
            "lat": "34.1526",
            "long": "77.5771",
            "confirmed": 16918,
            "recovered": 15158,
            "deaths": 171,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Lakshadweep": {
            "lat": "13.6999972",
            "long": "72.1833326",
            "confirmed": 5466,
            "recovered": 3974,
            "deaths": 18,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Madhya Pradesh": {
            "lat": "23.541513",
            "long": "78.289633",
            "confirmed": 747783,
            "recovered": 662949,
            "deaths": 7227,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Maharashtra": {
            "lat": "19.449759",
            "long": "76.108221",
            "confirmed": 5467537,
            "recovered": 4978937,
            "deaths": 84371,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Manipur": {
            "lat": "24.738975",
            "long": "93.882541",
            "confirmed": 41265,
            "recovered": 33813,
            "deaths": 635,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Meghalaya": {
            "lat": "25.536934",
            "long": "91.278882",
            "confirmed": 25744,
            "recovered": 19596,
            "deaths": 379,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mizoram": {
            "lat": "23.309381",
            "long": "92.83822",
            "confirmed": 9444,
            "recovered": 7271,
            "deaths": 30,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nagaland": {
            "lat": "26.06702",
            "long": "94.470302",
            "confirmed": 18958,
            "recovered": 14298,
            "deaths": 234,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Odisha": {
            "lat": "20.505428",
            "long": "84.418059",
            "confirmed": 644401,
            "recovered": 546631,
            "deaths": 2378,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Puducherry": {
            "lat": "11.882658",
            "long": "78.86498",
            "confirmed": 89508,
            "recovered": 70615,
            "deaths": 1241,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Punjab": {
            "lat": "30.841465",
            "long": "75.40879",
            "confirmed": 517954,
            "recovered": 434930,
            "deaths": 12525,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Rajasthan": {
            "lat": "26.583423",
            "long": "73.847973",
            "confirmed": 889513,
            "recovered": 729168,
            "deaths": 7219,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sikkim": {
            "lat": "27.571671",
            "long": "88.472712",
            "confirmed": 11955,
            "recovered": 8649,
            "deaths": 214,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tamil Nadu": {
            "lat": "11.006091",
            "long": "78.400624",
            "confirmed": 1699225,
            "recovered": 1426915,
            "deaths": 18734,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Telangana": {
            "lat": "18.1124",
            "long": "79.0193",
            "confirmed": 540603,
            "recovered": 490620,
            "deaths": 3037,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tripura": {
            "lat": "23.746783",
            "long": "91.743565",
            "confirmed": 43496,
            "recovered": 36718,
            "deaths": 453,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Uttar Pradesh": {
            "lat": "26.925425",
            "long": "80.560982",
            "confirmed": 1644849,
            "recovered": 1502918,
            "deaths": 18352,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Uttarakhand": {
            "lat": "30.156447",
            "long": "79.197608",
            "confirmed": 300282,
            "recovered": 221785,
            "deaths": 5325,
            "updated": "2021/05/20 17:21:11+00"
        },
        "West Bengal": {
            "lat": "23.814082",
            "long": "87.979803",
            "confirmed": 1190867,
            "recovered": 1045643,
            "deaths": 13733,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Indonesia": {
        "All": {
            "confirmed": 1758898,
            "recovered": 1621572,
            "deaths": 48887,
            "country": "Indonesia",
            "population": 263991379,
            "sq_km_area": 1904569,
            "life_expectancy": "70.1",
            "elevation_in_meters": 367,
            "continent": "Asia",
            "abbreviation": "ID",
            "location": "Southeast Asia",
            "iso": 360,
            "capital_city": "Jakarta",
            "lat": "-0.7893",
            "long": "113.9213",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Iran": {
        "All": {
            "confirmed": 2804632,
            "recovered": 2290613,
            "deaths": 77994,
            "country": "Iran",
            "population": 81162788,
            "sq_km_area": 1648195,
            "life_expectancy": "69.7",
            "elevation_in_meters": "1,305",
            "continent": "Asia",
            "abbreviation": "IR",
            "location": "Southern and Central Asia",
            "iso": 364,
            "capital_city": "Tehran",
            "lat": "32.427908",
            "long": "53.688046",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Iraq": {
        "All": {
            "confirmed": 1156137,
            "recovered": 1067387,
            "deaths": 16102,
            "country": "Iraq",
            "population": 38274618,
            "sq_km_area": 438317,
            "life_expectancy": "66.5",
            "elevation_in_meters": 312,
            "continent": "Asia",
            "abbreviation": "IQ",
            "location": "Middle East",
            "iso": 368,
            "capital_city": "Baghdad",
            "lat": "33.223191",
            "long": "43.679291",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Ireland": {
        "All": {
            "confirmed": 254870,
            "recovered": 23364,
            "deaths": 4941,
            "country": "Ireland",
            "population": 4761657,
            "sq_km_area": 70273,
            "life_expectancy": "76.8",
            "elevation_in_meters": 118,
            "continent": "Europe",
            "abbreviation": "IE",
            "location": "British Isles",
            "iso": 372,
            "capital_city": "Dublin",
            "lat": "53.1424",
            "long": "-7.6921",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Israel": {
        "All": {
            "confirmed": 839261,
            "recovered": 832242,
            "deaths": 6396,
            "country": "Israel",
            "population": 8321570,
            "sq_km_area": 21056,
            "life_expectancy": "78.6",
            "elevation_in_meters": 508,
            "continent": "Asia",
            "abbreviation": "IL",
            "location": "Middle East",
            "iso": 376,
            "capital_city": "Jerusalem",
            "lat": "31.046051",
            "long": "34.851612",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Italy": {
        "All": {
            "confirmed": 4178261,
            "recovered": 3753965,
            "deaths": 124810,
            "country": "Italy",
            "population": 59359900,
            "sq_km_area": 301316,
            "life_expectancy": 79,
            "elevation_in_meters": 538,
            "continent": "Europe",
            "abbreviation": "IT",
            "location": "Southern Europe",
            "iso": 380,
            "capital_city": "Roma"
        },
        "Abruzzo": {
            "lat": "42.35122196",
            "long": "13.39843823",
            "confirmed": 73427,
            "recovered": 65021,
            "deaths": 2463,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Basilicata": {
            "lat": "40.63947052",
            "long": "15.80514834",
            "confirmed": 25848,
            "recovered": 20560,
            "deaths": 569,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Calabria": {
            "lat": "38.90597598",
            "long": "16.59440194",
            "confirmed": 65302,
            "recovered": 53052,
            "deaths": 1127,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Campania": {
            "lat": "40.83956555",
            "long": "14.25084984",
            "confirmed": 414067,
            "recovered": 331774,
            "deaths": 6933,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Emilia-Romagna": {
            "lat": "44.49436681",
            "long": "11.3417208",
            "confirmed": 381029,
            "recovered": 346200,
            "deaths": 13122,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Friuli Venezia Giulia": {
            "lat": "45.6494354",
            "long": "13.76813649",
            "confirmed": 106660,
            "recovered": 97485,
            "deaths": 3775,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Lazio": {
            "lat": "41.89277044",
            "long": "12.48366722",
            "confirmed": 338234,
            "recovered": 300033,
            "deaths": 8063,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Liguria": {
            "lat": "44.41149315",
            "long": "8.9326992",
            "confirmed": 102109,
            "recovered": 95483,
            "deaths": 4299,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Lombardia": {
            "lat": "45.46679409",
            "long": "9.190347404",
            "confirmed": 828701,
            "recovered": 758880,
            "deaths": 33438,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Marche": {
            "lat": "43.61675973",
            "long": "13.5188753",
            "confirmed": 101536,
            "recovered": 93966,
            "deaths": 2998,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Molise": {
            "lat": "41.55774754",
            "long": "14.65916051",
            "confirmed": 13535,
            "recovered": 12801,
            "deaths": 488,
            "updated": "2021/05/20 17:21:11+00"
        },
        "P.A. Bolzano": {
            "lat": "46.49933453",
            "long": "11.35662422",
            "confirmed": 72440,
            "recovered": 70335,
            "deaths": 1170,
            "updated": "2021/05/20 17:21:11+00"
        },
        "P.A. Trento": {
            "lat": "46.06893511",
            "long": "11.12123097",
            "confirmed": 45067,
            "recovered": 43050,
            "deaths": 1354,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Piemonte": {
            "lat": "45.0732745",
            "long": "7.680687483",
            "confirmed": 357395,
            "recovered": 336799,
            "deaths": 11559,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Puglia": {
            "lat": "41.12559576",
            "long": "16.86736689",
            "confirmed": 247720,
            "recovered": 207173,
            "deaths": 6342,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sardegna": {
            "lat": "39.21531192",
            "long": "9.110616306",
            "confirmed": 56332,
            "recovered": 41221,
            "deaths": 1442,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sicilia": {
            "lat": "38.11569725",
            "long": "13.3623567",
            "confirmed": 221811,
            "recovered": 201089,
            "deaths": 5709,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Toscana": {
            "lat": "43.76923077",
            "long": "11.25588885",
            "confirmed": 238342,
            "recovered": 218890,
            "deaths": 6590,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Umbria": {
            "lat": "43.10675841",
            "long": "12.38824698",
            "confirmed": 55980,
            "recovered": 52403,
            "deaths": 1383,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Valle d'Aosta": {
            "lat": "45.73750286",
            "long": "7.320149366",
            "confirmed": 11443,
            "recovered": 10628,
            "deaths": 471,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Veneto": {
            "lat": "45.43490485",
            "long": "12.33845213",
            "confirmed": 421283,
            "recovered": 397122,
            "deaths": 11515,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Jamaica": {
        "All": {
            "confirmed": 47672,
            "recovered": 23643,
            "deaths": 902,
            "country": "Jamaica",
            "population": 2890299,
            "sq_km_area": 10990,
            "life_expectancy": "75.2",
            "elevation_in_meters": 18,
            "continent": "North America",
            "abbreviation": "JM",
            "location": "Caribbean",
            "iso": 388,
            "capital_city": "Kingston",
            "lat": "18.1096",
            "long": "-77.2975",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Japan": {
        "All": {
            "confirmed": 705581,
            "recovered": 615168,
            "deaths": 12006,
            "country": "Japan",
            "population": 127484450,
            "sq_km_area": 377829,
            "life_expectancy": "80.7",
            "elevation_in_meters": 438,
            "continent": "Asia",
            "abbreviation": "JP",
            "location": "Eastern Asia",
            "iso": 392,
            "capital_city": "Tokyo"
        },
        "Aichi": {
            "lat": "35.035551",
            "long": "137.211621",
            "confirmed": 42980,
            "recovered": 35091,
            "deaths": 677,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Akita": {
            "lat": "39.748679",
            "long": "140.408228",
            "confirmed": 727,
            "recovered": 584,
            "deaths": 11,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Aomori": {
            "lat": "40.781541",
            "long": "140.828896",
            "confirmed": 2099,
            "recovered": 1740,
            "deaths": 26,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chiba": {
            "lat": "35.510141",
            "long": "140.198917",
            "confirmed": 35897,
            "recovered": 33894,
            "deaths": 658,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ehime": {
            "lat": "33.624835",
            "long": "132.856842",
            "confirmed": 2650,
            "recovered": 2426,
            "deaths": 67,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Fukui": {
            "lat": "35.846614",
            "long": "136.224654",
            "confirmed": 1008,
            "recovered": 913,
            "deaths": 34,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Fukuoka": {
            "lat": "33.526032",
            "long": "130.666949",
            "confirmed": 31791,
            "recovered": 24637,
            "deaths": 408,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Fukushima": {
            "lat": "37.378867",
            "long": "140.223295",
            "confirmed": 4368,
            "recovered": 3685,
            "deaths": 130,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gifu": {
            "lat": "35.778671",
            "long": "137.055925",
            "confirmed": 7889,
            "recovered": 6666,
            "deaths": 143,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gunma": {
            "lat": "36.504479",
            "long": "138.985605",
            "confirmed": 7434,
            "recovered": 6598,
            "deaths": 122,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hiroshima": {
            "lat": "34.605309",
            "long": "132.788719",
            "confirmed": 9218,
            "recovered": 6612,
            "deaths": 115,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hokkaido": {
            "lat": "43.385711",
            "long": "142.552318",
            "confirmed": 32585,
            "recovered": 24766,
            "deaths": 973,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hyogo": {
            "lat": "35.039913",
            "long": "134.828057",
            "confirmed": 38495,
            "recovered": 33204,
            "deaths": 1049,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ibaraki": {
            "lat": "36.303588",
            "long": "140.319591",
            "confirmed": 9255,
            "recovered": 8481,
            "deaths": 139,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ishikawa": {
            "lat": "36.769464",
            "long": "136.771027",
            "confirmed": 3360,
            "recovered": 2650,
            "deaths": 99,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Iwate": {
            "lat": "39.593287",
            "long": "141.361777",
            "confirmed": 1300,
            "recovered": 1047,
            "deaths": 42,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kagawa": {
            "lat": "34.217292",
            "long": "133.969047",
            "confirmed": 1884,
            "recovered": 1482,
            "deaths": 24,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kagoshima": {
            "lat": "31.009484",
            "long": "130.430665",
            "confirmed": 3025,
            "recovered": 2519,
            "deaths": 30,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kanagawa": {
            "lat": "35.415312",
            "long": "139.338983",
            "confirmed": 58956,
            "recovered": 55411,
            "deaths": 862,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kochi": {
            "lat": "33.422519",
            "long": "133.367307",
            "confirmed": 1201,
            "recovered": 1096,
            "deaths": 20,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kumamoto": {
            "lat": "32.608154",
            "long": "130.745231",
            "confirmed": 5776,
            "recovered": 4484,
            "deaths": 90,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Kyoto": {
            "lat": "35.253815",
            "long": "135.443341",
            "confirmed": 15152,
            "recovered": 13182,
            "deaths": 198,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mie": {
            "lat": "34.508018",
            "long": "136.376013",
            "confirmed": 4616,
            "recovered": 4003,
            "deaths": 99,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Miyagi": {
            "lat": "38.446859",
            "long": "140.927086",
            "confirmed": 8714,
            "recovered": 8214,
            "deaths": 81,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Miyazaki": {
            "lat": "32.193204",
            "long": "131.299374",
            "confirmed": 2918,
            "recovered": 2499,
            "deaths": 24,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nagano": {
            "lat": "36.132134",
            "long": "138.045528",
            "confirmed": 4499,
            "recovered": 4089,
            "deaths": 75,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nagasaki": {
            "lat": "33.235712",
            "long": "129.608033",
            "confirmed": 2804,
            "recovered": 2304,
            "deaths": 57,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nara": {
            "lat": "34.317451",
            "long": "135.871644",
            "confirmed": 7531,
            "recovered": 6750,
            "deaths": 101,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Niigata": {
            "lat": "37.521819",
            "long": "138.918647",
            "confirmed": 2955,
            "recovered": 2485,
            "deaths": 33,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Oita": {
            "lat": "33.200697",
            "long": "131.43324",
            "confirmed": 3055,
            "recovered": 2313,
            "deaths": 37,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Okayama": {
            "lat": "34.89246",
            "long": "133.826252",
            "confirmed": 6631,
            "recovered": 4071,
            "deaths": 80,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Okinawa": {
            "lat": "25.768923",
            "long": "126.668016",
            "confirmed": 14545,
            "recovered": 12510,
            "deaths": 148,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Osaka": {
            "lat": "34.620965",
            "long": "135.507481",
            "confirmed": 96632,
            "recovered": 78633,
            "deaths": 2064,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Port Quarantine": {
            "lat": "",
            "long": "",
            "confirmed": 2899,
            "recovered": 2785,
            "deaths": 3,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saga": {
            "lat": "33.286977",
            "long": "130.115738",
            "confirmed": 2355,
            "recovered": 1950,
            "deaths": 18,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Saitama": {
            "lat": "35.997101",
            "long": "139.347635",
            "confirmed": 42345,
            "recovered": 38754,
            "deaths": 771,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shiga": {
            "lat": "35.215827",
            "long": "136.138064",
            "confirmed": 4691,
            "recovered": 4028,
            "deaths": 74,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shimane": {
            "lat": "35.07076",
            "long": "132.554064",
            "confirmed": 494,
            "recovered": 391,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Shizuoka": {
            "lat": "34.916975",
            "long": "138.407784",
            "confirmed": 7724,
            "recovered": 6690,
            "deaths": 130,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tochigi": {
            "lat": "36.689912",
            "long": "139.819213",
            "confirmed": 5993,
            "recovered": 5514,
            "deaths": 74,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tokushima": {
            "lat": "33.919178",
            "long": "134.242091",
            "confirmed": 1599,
            "recovered": 1395,
            "deaths": 57,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tokyo": {
            "lat": "35.711343",
            "long": "139.446921",
            "confirmed": 155067,
            "recovered": 145819,
            "deaths": 1981,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tottori": {
            "lat": "35.359069",
            "long": "133.863619",
            "confirmed": 445,
            "recovered": 382,
            "deaths": 2,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Toyama": {
            "lat": "36.637464",
            "long": "137.269346",
            "confirmed": 1619,
            "recovered": 1344,
            "deaths": 34,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 8,
            "recovered": 8,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Wakayama": {
            "lat": "33.911879",
            "long": "135.505446",
            "confirmed": 2527,
            "recovered": 2281,
            "deaths": 34,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yamagata": {
            "lat": "38.448396",
            "long": "140.102154",
            "confirmed": 1805,
            "recovered": 1543,
            "deaths": 38,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yamaguchi": {
            "lat": "34.20119",
            "long": "131.573293",
            "confirmed": 2610,
            "recovered": 1961,
            "deaths": 55,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yamanashi": {
            "lat": "35.612364",
            "long": "138.611489",
            "confirmed": 1450,
            "recovered": 1284,
            "deaths": 19,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Jordan": {
        "All": {
            "confirmed": 726432,
            "recovered": 710445,
            "deaths": 9295,
            "country": "Jordan",
            "population": 9702353,
            "sq_km_area": 88946,
            "life_expectancy": "77.4",
            "elevation_in_meters": 812,
            "continent": "Asia",
            "abbreviation": "JO",
            "location": "Middle East",
            "iso": 400,
            "capital_city": "Amman",
            "lat": "31.24",
            "long": "36.51",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Kazakhstan": {
        "All": {
            "confirmed": 420863,
            "recovered": 380007,
            "deaths": 3409,
            "country": "Kazakhstan",
            "population": 18204499,
            "sq_km_area": 2724900,
            "life_expectancy": "63.2",
            "elevation_in_meters": null,
            "continent": "Asia",
            "abbreviation": "KZ",
            "location": "Southern and Central Asia",
            "iso": null,
            "capital_city": "Astana",
            "lat": "48.0196",
            "long": "66.9237",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Kenya": {
        "All": {
            "confirmed": 166876,
            "recovered": 114285,
            "deaths": 3040,
            "country": "Kenya",
            "population": 49699862,
            "sq_km_area": 580367,
            "life_expectancy": 48,
            "elevation_in_meters": 762,
            "continent": "Africa",
            "abbreviation": "KE",
            "location": "Eastern Africa",
            "iso": 404,
            "capital_city": "Nairobi",
            "lat": "-0.0236",
            "long": "37.9062",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Korea, South": {
        "All": {
            "confirmed": 134117,
            "recovered": 123659,
            "deaths": 1916,
            "country": "Korea, South",
            "population": 50982212,
            "sq_km_area": 99434,
            "life_expectancy": "74.4",
            "elevation_in_meters": 282,
            "continent": "Asia",
            "abbreviation": "KR",
            "location": "Eastern Asia",
            "iso": 410,
            "capital_city": "Seoul",
            "lat": "35.907757",
            "long": "127.766922",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Kosovo": {
        "All": {
            "confirmed": 106712,
            "recovered": 100727,
            "deaths": 2242,
            "lat": "42.602636",
            "long": "20.902977",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Kuwait": {
        "All": {
            "confirmed": 294693,
            "recovered": 280940,
            "deaths": 1703,
            "country": "Kuwait",
            "population": 4136528,
            "sq_km_area": 17818,
            "life_expectancy": "76.1",
            "elevation_in_meters": 108,
            "continent": "Asia",
            "abbreviation": "KW",
            "location": "Middle East",
            "iso": 414,
            "capital_city": "Kuwait",
            "lat": "29.31166",
            "long": "47.481766",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Kyrgyzstan": {
        "All": {
            "confirmed": 101580,
            "recovered": 94803,
            "deaths": 1729,
            "country": "Kyrgyzstan",
            "population": 6045117,
            "sq_km_area": 199900,
            "life_expectancy": "63.4",
            "elevation_in_meters": "2,988",
            "continent": "Asia",
            "abbreviation": "KG",
            "location": "Southern and Central Asia",
            "iso": 417,
            "capital_city": "Bishkek",
            "lat": "41.20438",
            "long": "74.766098",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Laos": {
        "All": {
            "confirmed": 1751,
            "recovered": 740,
            "deaths": 2,
            "country": "Laos",
            "population": 6858160,
            "sq_km_area": 236800,
            "life_expectancy": "53.1",
            "elevation_in_meters": 710,
            "continent": "Asia",
            "abbreviation": "LA",
            "location": "Southeast Asia",
            "iso": 418,
            "capital_city": "Vientiane",
            "lat": "19.85627",
            "long": "102.495496",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Latvia": {
        "All": {
            "confirmed": 129794,
            "recovered": 118750,
            "deaths": 2301,
            "country": "Latvia",
            "population": 1949670,
            "sq_km_area": 64589,
            "life_expectancy": "68.4",
            "elevation_in_meters": 87,
            "continent": "Europe",
            "abbreviation": "LV",
            "location": "Baltic Countries",
            "iso": 428,
            "capital_city": "Riga",
            "lat": "56.8796",
            "long": "24.6032",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Lebanon": {
        "All": {
            "confirmed": 537043,
            "recovered": 503774,
            "deaths": 7651,
            "country": "Lebanon",
            "population": 6082357,
            "sq_km_area": 10400,
            "life_expectancy": "71.3",
            "elevation_in_meters": "1,250",
            "continent": "Asia",
            "abbreviation": "LB",
            "location": "Middle East",
            "iso": 422,
            "capital_city": "Beirut",
            "lat": "33.8547",
            "long": "35.8623",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Lesotho": {
        "All": {
            "confirmed": 10806,
            "recovered": 6427,
            "deaths": 320,
            "country": "Lesotho",
            "population": 2233339,
            "sq_km_area": 30355,
            "life_expectancy": "50.8",
            "elevation_in_meters": "2,161",
            "continent": "Africa",
            "abbreviation": "LS",
            "location": "Southern Africa",
            "iso": 426,
            "capital_city": "Maseru",
            "lat": "-29.61",
            "long": "28.2336",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Liberia": {
        "All": {
            "confirmed": 2142,
            "recovered": 2022,
            "deaths": 85,
            "country": "Liberia",
            "population": 4731906,
            "sq_km_area": 111369,
            "life_expectancy": 51,
            "elevation_in_meters": 243,
            "continent": "Africa",
            "abbreviation": "LR",
            "location": "Western Africa",
            "iso": 430,
            "capital_city": "Monrovia",
            "lat": "6.428055",
            "long": "-9.429499",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Libya": {
        "All": {
            "confirmed": 182649,
            "recovered": 169364,
            "deaths": 3102,
            "country": "Libya",
            "population": 5605000,
            "sq_km_area": 1759540,
            "life_expectancy": "75.5",
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "LY",
            "location": "Northern Africa",
            "iso": null,
            "capital_city": "Tripoli",
            "lat": "26.3351",
            "long": "17.228331",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Liechtenstein": {
        "All": {
            "confirmed": 2993,
            "recovered": 2906,
            "deaths": 58,
            "country": "Liechtenstein",
            "population": 37922,
            "sq_km_area": 160,
            "life_expectancy": "78.8",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "LI",
            "location": "Western Europe",
            "iso": 438,
            "capital_city": "Vaduz",
            "lat": "47.14",
            "long": "9.55",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Lithuania": {
        "All": {
            "confirmed": 268684,
            "recovered": 244352,
            "deaths": 4155,
            "country": "Lithuania",
            "population": 2890297,
            "sq_km_area": 65301,
            "life_expectancy": "69.1",
            "elevation_in_meters": 110,
            "continent": "Europe",
            "abbreviation": "LT",
            "location": "Baltic Countries",
            "iso": 440,
            "capital_city": "Vilnius",
            "lat": "55.1694",
            "long": "23.8813",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Luxembourg": {
        "All": {
            "confirmed": 69465,
            "recovered": 1662,
            "deaths": 809,
            "country": "Luxembourg",
            "population": 583455,
            "sq_km_area": 2586,
            "life_expectancy": "77.1",
            "elevation_in_meters": 325,
            "continent": "Europe",
            "abbreviation": "LU",
            "location": "Western Europe",
            "iso": 442,
            "capital_city": "Luxembourg [Luxemburg/L",
            "lat": "49.8153",
            "long": "6.1296",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "MS Zaandam": {
        "All": {
            "confirmed": 9,
            "recovered": 7,
            "deaths": 2,
            "lat": "",
            "long": "",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Madagascar": {
        "All": {
            "confirmed": 40519,
            "recovered": 38679,
            "deaths": 783,
            "country": "Madagascar",
            "population": 25570895,
            "sq_km_area": 587041,
            "life_expectancy": 55,
            "elevation_in_meters": 615,
            "continent": "Africa",
            "abbreviation": "MG",
            "location": "Eastern Africa",
            "iso": 450,
            "capital_city": "Antananarivo",
            "lat": "-18.766947",
            "long": "46.869107",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Malawi": {
        "All": {
            "confirmed": 34238,
            "recovered": 32246,
            "deaths": 1153,
            "country": "Malawi",
            "population": 18622104,
            "sq_km_area": 118484,
            "life_expectancy": "37.6",
            "elevation_in_meters": 779,
            "continent": "Africa",
            "abbreviation": "MW",
            "location": "Eastern Africa",
            "iso": 454,
            "capital_city": "Lilongwe",
            "lat": "-13.2543",
            "long": "34.3015",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Malaysia": {
        "All": {
            "confirmed": 492302,
            "recovered": 440032,
            "deaths": 2099,
            "country": "Malaysia",
            "population": 31624264,
            "sq_km_area": 329758,
            "life_expectancy": "70.8",
            "elevation_in_meters": 538,
            "continent": "Asia",
            "abbreviation": "MY",
            "location": "Southeast Asia",
            "iso": 458,
            "capital_city": "Kuala Lumpur",
            "lat": "4.210484",
            "long": "101.975766",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Maldives": {
        "All": {
            "confirmed": 48608,
            "recovered": 29644,
            "deaths": 113,
            "country": "Maldives",
            "population": 436330,
            "sq_km_area": 298,
            "life_expectancy": "62.2",
            "elevation_in_meters": "1.8",
            "continent": "Asia",
            "abbreviation": "MV",
            "location": "Southern and Central Asia",
            "iso": 462,
            "capital_city": "Male",
            "lat": "3.2028",
            "long": "73.2207",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mali": {
        "All": {
            "confirmed": 14221,
            "recovered": 9347,
            "deaths": 512,
            "country": "Mali",
            "population": 18541980,
            "sq_km_area": 1240192,
            "life_expectancy": "46.7",
            "elevation_in_meters": 343,
            "continent": "Africa",
            "abbreviation": "ML",
            "location": "Western Africa",
            "iso": 466,
            "capital_city": "Bamako",
            "lat": "17.570692",
            "long": "-3.996166",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Malta": {
        "All": {
            "confirmed": 30491,
            "recovered": 29974,
            "deaths": 417,
            "country": "Malta",
            "population": 430835,
            "sq_km_area": 316,
            "life_expectancy": "77.9",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "MT",
            "location": "Southern Europe",
            "iso": 470,
            "capital_city": "Valletta",
            "lat": "35.9375",
            "long": "14.3754",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Marshall Islands": {
        "All": {
            "confirmed": 4,
            "recovered": 4,
            "deaths": 0,
            "country": "Marshall Islands",
            "population": 53127,
            "sq_km_area": 181,
            "life_expectancy": "65.5",
            "elevation_in_meters": null,
            "continent": "Oceania",
            "abbreviation": "MH",
            "location": "Micronesia",
            "iso": 584,
            "capital_city": "Dalap-Uliga-Darrit",
            "lat": "7.1315",
            "long": "171.1845",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mauritania": {
        "All": {
            "confirmed": 18957,
            "recovered": 18197,
            "deaths": 457,
            "country": "Mauritania",
            "population": 4420184,
            "sq_km_area": 1025520,
            "life_expectancy": "50.8",
            "elevation_in_meters": 276,
            "continent": "Africa",
            "abbreviation": "MR",
            "location": "Western Africa",
            "iso": 478,
            "capital_city": "Nouakchott",
            "lat": "21.0079",
            "long": "-10.9408",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mauritius": {
        "All": {
            "confirmed": 1288,
            "recovered": 1145,
            "deaths": 17,
            "country": "Mauritius",
            "population": 1265138,
            "sq_km_area": 2040,
            "life_expectancy": 71,
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "MU",
            "location": "Eastern Africa",
            "iso": 480,
            "capital_city": "Port-Louis",
            "lat": "-20.348404",
            "long": "57.552152",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mexico": {
        "All": {
            "confirmed": 2387512,
            "recovered": 1907538,
            "deaths": 220850,
            "country": "Mexico",
            "population": 129163276,
            "sq_km_area": 1958201,
            "life_expectancy": "71.5",
            "elevation_in_meters": "1,111",
            "continent": "North America",
            "abbreviation": "MX",
            "location": "Central America",
            "iso": 484,
            "capital_city": "Ciudad de M",
            "lat": "19.4969",
            "long": "-99.7233"
        },
        "Aguascalientes": {
            "lat": "21.8853",
            "long": "-102.2916",
            "confirmed": 26375,
            "recovered": 0,
            "deaths": 2403,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Baja California": {
            "lat": "30.8406",
            "long": "-115.2838",
            "confirmed": 48621,
            "recovered": 0,
            "deaths": 8170,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Baja California Sur": {
            "lat": "26.0444",
            "long": "-111.6661",
            "confirmed": 31171,
            "recovered": 0,
            "deaths": 1393,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Campeche": {
            "lat": "19.8301",
            "long": "-90.5349",
            "confirmed": 9895,
            "recovered": 0,
            "deaths": 1218,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chiapas": {
            "lat": "16.7569",
            "long": "-93.1292",
            "confirmed": 11244,
            "recovered": 0,
            "deaths": 1563,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Chihuahua": {
            "lat": "28.633",
            "long": "-106.0691",
            "confirmed": 54696,
            "recovered": 0,
            "deaths": 6887,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ciudad de Mexico": {
            "lat": "19.4326",
            "long": "-99.1332",
            "confirmed": 651967,
            "recovered": 0,
            "deaths": 33045,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Coahuila": {
            "lat": "27.0587",
            "long": "-101.7068",
            "confirmed": 68513,
            "recovered": 0,
            "deaths": 6246,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Colima": {
            "lat": "19.1223",
            "long": "-104.0072",
            "confirmed": 11691,
            "recovered": 0,
            "deaths": 1187,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Durango": {
            "lat": "24.5593",
            "long": "-104.6588",
            "confirmed": 33961,
            "recovered": 0,
            "deaths": 2442,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guanajuato": {
            "lat": "21.019",
            "long": "-101.2574",
            "confirmed": 131493,
            "recovered": 0,
            "deaths": 10728,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Guerrero": {
            "lat": "17.4392",
            "long": "-99.5451",
            "confirmed": 40691,
            "recovered": 0,
            "deaths": 4423,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Hidalgo": {
            "lat": "20.0911",
            "long": "-98.7624",
            "confirmed": 38702,
            "recovered": 0,
            "deaths": 6174,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Jalisco": {
            "lat": "20.6595",
            "long": "-103.3494",
            "confirmed": 86507,
            "recovered": 0,
            "deaths": 12078,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Mexico": {
            "lat": "19.4969",
            "long": "-99.7233",
            "confirmed": 248057,
            "recovered": 0,
            "deaths": 34543,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Michoacan": {
            "lat": "19.5665",
            "long": "-101.7068",
            "confirmed": 47962,
            "recovered": 0,
            "deaths": 5749,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Morelos": {
            "lat": "18.6813",
            "long": "-99.1013",
            "confirmed": 33243,
            "recovered": 0,
            "deaths": 3273,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nayarit": {
            "lat": "21.7514",
            "long": "-104.8455",
            "confirmed": 12043,
            "recovered": 0,
            "deaths": 1815,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Nuevo Leon": {
            "lat": "25.5922",
            "long": "-99.9962",
            "confirmed": 123598,
            "recovered": 0,
            "deaths": 9463,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Oaxaca": {
            "lat": "17.0732",
            "long": "-96.7266",
            "confirmed": 46833,
            "recovered": 0,
            "deaths": 3651,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Puebla": {
            "lat": "19.0414",
            "long": "-98.2063",
            "confirmed": 84529,
            "recovered": 0,
            "deaths": 11699,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Queretaro": {
            "lat": "20.5888",
            "long": "-100.3899",
            "confirmed": 68726,
            "recovered": 0,
            "deaths": 4278,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Quintana Roo": {
            "lat": "19.1817",
            "long": "-88.4791",
            "confirmed": 25417,
            "recovered": 0,
            "deaths": 2701,
            "updated": "2021/05/20 17:21:11+00"
        },
        "San Luis Potosi": {
            "lat": "22.1565",
            "long": "-100.9855",
            "confirmed": 63767,
            "recovered": 0,
            "deaths": 5261,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sinaloa": {
            "lat": "25.1721",
            "long": "-107.4795",
            "confirmed": 38553,
            "recovered": 0,
            "deaths": 6101,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sonora": {
            "lat": "29.2972",
            "long": "-110.3309",
            "confirmed": 74445,
            "recovered": 0,
            "deaths": 6579,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tabasco": {
            "lat": "17.8409",
            "long": "-92.6189",
            "confirmed": 66838,
            "recovered": 0,
            "deaths": 4118,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tamaulipas": {
            "lat": "24.2669",
            "long": "-98.8363",
            "confirmed": 58340,
            "recovered": 0,
            "deaths": 4925,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tlaxcala": {
            "lat": "19.3139",
            "long": "-98.2404",
            "confirmed": 19814,
            "recovered": 0,
            "deaths": 2422,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 1907538,
            "deaths": 0,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Veracruz": {
            "lat": "19.1738",
            "long": "-96.1342",
            "confirmed": 60749,
            "recovered": 0,
            "deaths": 9757,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Yucatan": {
            "lat": "20.7099",
            "long": "-89.0943",
            "confirmed": 38506,
            "recovered": 0,
            "deaths": 3781,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Zacatecas": {
            "lat": "22.7709",
            "long": "-102.5832",
            "confirmed": 30565,
            "recovered": 0,
            "deaths": 2777,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Micronesia": {
        "All": {
            "confirmed": 1,
            "recovered": 1,
            "deaths": 0,
            "lat": "7.4256",
            "long": "150.5508",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Moldova": {
        "All": {
            "confirmed": 254379,
            "recovered": 245803,
            "deaths": 6061,
            "country": "Moldova",
            "population": 4051212,
            "sq_km_area": 33851,
            "life_expectancy": "64.5",
            "elevation_in_meters": 139,
            "continent": "Europe",
            "abbreviation": "MD",
            "location": "Eastern Europe",
            "iso": 498,
            "capital_city": "Chisinau",
            "lat": "47.4116",
            "long": "28.3699",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Monaco": {
        "All": {
            "confirmed": 2497,
            "recovered": 2450,
            "deaths": 32,
            "country": "Monaco",
            "population": 38695,
            "sq_km_area": 1.5,
            "life_expectancy": "78.8",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "MC",
            "location": "Western Europe",
            "iso": 492,
            "capital_city": "Monaco-Ville",
            "lat": "43.7333",
            "long": "7.4167",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mongolia": {
        "All": {
            "confirmed": 50618,
            "recovered": 44391,
            "deaths": 235,
            "country": "Mongolia",
            "population": 3075647,
            "sq_km_area": 1566500,
            "life_expectancy": "67.3",
            "elevation_in_meters": "1,528",
            "continent": "Asia",
            "abbreviation": "MN",
            "location": "Eastern Asia",
            "iso": 496,
            "capital_city": "Ulan Bator",
            "lat": "46.8625",
            "long": "103.8467",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Montenegro": {
        "All": {
            "confirmed": 99098,
            "recovered": 96340,
            "deaths": 1569,
            "lat": "42.708678",
            "long": "19.37439",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Morocco": {
        "All": {
            "confirmed": 515758,
            "recovered": 504210,
            "deaths": 9106,
            "country": "Morocco",
            "population": 35739580,
            "sq_km_area": 446550,
            "life_expectancy": "69.1",
            "elevation_in_meters": 909,
            "continent": "Africa",
            "abbreviation": "MA",
            "location": "Northern Africa",
            "iso": 504,
            "capital_city": "Rabat",
            "lat": "31.7917",
            "long": "-7.0926",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Mozambique": {
        "All": {
            "confirmed": 70509,
            "recovered": 68777,
            "deaths": 828,
            "country": "Mozambique",
            "population": 29668834,
            "sq_km_area": 801590,
            "life_expectancy": "37.5",
            "elevation_in_meters": 345,
            "continent": "Africa",
            "abbreviation": "MZ",
            "location": "Eastern Africa",
            "iso": 508,
            "capital_city": "Maputo",
            "lat": "-18.665695",
            "long": "35.529562",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Namibia": {
        "All": {
            "confirmed": 51827,
            "recovered": 48531,
            "deaths": 739,
            "country": "Namibia",
            "population": 2533794,
            "sq_km_area": 824292,
            "life_expectancy": "42.5",
            "elevation_in_meters": "1,141",
            "continent": "Africa",
            "abbreviation": "NA",
            "location": "Southern Africa",
            "iso": 516,
            "capital_city": "Windhoek",
            "lat": "-22.9576",
            "long": "18.4904",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Nepal": {
        "All": {
            "confirmed": 488645,
            "recovered": 366946,
            "deaths": 5847,
            "country": "Nepal",
            "population": 29304998,
            "sq_km_area": 147181,
            "life_expectancy": "57.8",
            "elevation_in_meters": "3,265",
            "continent": "Asia",
            "abbreviation": "NP",
            "location": "Southern and Central Asia",
            "iso": 524,
            "capital_city": "Kathmandu",
            "lat": "28.1667",
            "long": "84.25",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Netherlands": {
        "All": {
            "confirmed": 1642573,
            "recovered": 26590,
            "deaths": 17776,
            "country": "Netherlands",
            "population": 17035938,
            "sq_km_area": 41526,
            "life_expectancy": "78.3",
            "elevation_in_meters": 30,
            "continent": "Europe",
            "abbreviation": "NL",
            "location": "Western Europe",
            "iso": 528,
            "capital_city": "Amsterdam"
        },
        "Aruba": {
            "lat": "12.5211",
            "long": "-69.9683",
            "confirmed": 10879,
            "recovered": 10704,
            "deaths": 105,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Bonaire, Sint Eustatius and Saba": {
            "lat": "12.1784",
            "long": "-68.2385",
            "confirmed": 1606,
            "recovered": 1570,
            "deaths": 17,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Curacao": {
            "lat": "12.1696",
            "long": "-68.99",
            "confirmed": 12262,
            "recovered": 12080,
            "deaths": 121,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Drenthe": {
            "lat": "52.862485",
            "long": "6.618435",
            "confirmed": 32858,
            "recovered": 0,
            "deaths": 351,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Flevoland": {
            "lat": "52.550383",
            "long": "5.515162",
            "confirmed": 35736,
            "recovered": 0,
            "deaths": 240,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Friesland": {
            "lat": "53.087337",
            "long": "5.7925",
            "confirmed": 42073,
            "recovered": 0,
            "deaths": 473,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gelderland": {
            "lat": "52.061738",
            "long": "5.939114",
            "confirmed": 180466,
            "recovered": 0,
            "deaths": 2008,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Groningen": {
            "lat": "53.217922",
            "long": "6.741514",
            "confirmed": 37720,
            "recovered": 0,
            "deaths": 259,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Limburg": {
            "lat": "51.209227",
            "long": "5.93387",
            "confirmed": 110718,
            "recovered": 0,
            "deaths": 1624,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Noord-Brabant": {
            "lat": "51.561174",
            "long": "5.184942",
            "confirmed": 261477,
            "recovered": 0,
            "deaths": 3114,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Noord-Holland": {
            "lat": "52.600906",
            "long": "4.918688",
            "confirmed": 268573,
            "recovered": 0,
            "deaths": 2573,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Overijssel": {
            "lat": "52.444558",
            "long": "6.441722",
            "confirmed": 105358,
            "recovered": 0,
            "deaths": 1122,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sint Maarten": {
            "lat": "18.0425",
            "long": "-63.0548",
            "confirmed": 2326,
            "recovered": 2236,
            "deaths": 27,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 3882,
            "recovered": 0,
            "deaths": 14,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Utrecht": {
            "lat": "52.084251",
            "long": "5.163824",
            "confirmed": 123538,
            "recovered": 0,
            "deaths": 1270,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Zeeland": {
            "lat": "51.47936",
            "long": "3.861559",
            "confirmed": 28610,
            "recovered": 0,
            "deaths": 240,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Zuid-Holland": {
            "lat": "51.937835",
            "long": "4.462114",
            "confirmed": 384491,
            "recovered": 0,
            "deaths": 4218,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "New Zealand": {
        "All": {
            "confirmed": 2659,
            "recovered": 2609,
            "deaths": 26,
            "country": "New Zealand",
            "population": 4705818,
            "sq_km_area": 270534,
            "life_expectancy": "77.8",
            "elevation_in_meters": 388,
            "continent": "Oceania",
            "abbreviation": "NZ",
            "location": "Australia and New Zealand",
            "iso": 554,
            "capital_city": "Wellington",
            "lat": "-40.9006",
            "long": "174.886",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Nicaragua": {
        "All": {
            "confirmed": 7193,
            "recovered": 4225,
            "deaths": 185,
            "country": "Nicaragua",
            "population": 6217581,
            "sq_km_area": 130000,
            "life_expectancy": "68.7",
            "elevation_in_meters": 298,
            "continent": "North America",
            "abbreviation": "NI",
            "location": "Central America",
            "iso": 558,
            "capital_city": "Managua",
            "lat": "12.865416",
            "long": "-85.207229",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Niger": {
        "All": {
            "confirmed": 5356,
            "recovered": 5008,
            "deaths": 192,
            "country": "Niger",
            "population": 21477348,
            "sq_km_area": 1267000,
            "life_expectancy": "41.3",
            "elevation_in_meters": 474,
            "continent": "Africa",
            "abbreviation": "NE",
            "location": "Western Africa",
            "iso": 562,
            "capital_city": "Niamey",
            "lat": "17.607789",
            "long": "8.081666",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Nigeria": {
        "All": {
            "confirmed": 165852,
            "recovered": 156456,
            "deaths": 2067,
            "country": "Nigeria",
            "population": 190886311,
            "sq_km_area": 923768,
            "life_expectancy": "51.6",
            "elevation_in_meters": 380,
            "continent": "Africa",
            "abbreviation": "NG",
            "location": "Western Africa",
            "iso": 566,
            "capital_city": "Abuja",
            "lat": "9.082",
            "long": "8.6753",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "North Macedonia": {
        "All": {
            "confirmed": 154937,
            "recovered": 146632,
            "deaths": 5265,
            "country": "North Macedonia",
            "population": 2024000,
            "sq_km_area": 25713,
            "life_expectancy": "73.8",
            "elevation_in_meters": 741,
            "continent": "Europe",
            "abbreviation": "MK",
            "location": "Southern Europe",
            "iso": 807,
            "capital_city": "Skopje",
            "lat": "41.6086",
            "long": "21.7453",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Norway": {
        "All": {
            "confirmed": 120513,
            "recovered": 17998,
            "deaths": 781,
            "country": "Norway",
            "population": 5305383,
            "sq_km_area": 323877,
            "life_expectancy": "78.7",
            "elevation_in_meters": 460,
            "continent": "Europe",
            "abbreviation": "NO",
            "location": "Nordic Countries",
            "iso": 578,
            "capital_city": "Oslo",
            "lat": "60.472",
            "long": "8.4689",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Oman": {
        "All": {
            "confirmed": 208607,
            "recovered": 192973,
            "deaths": 2239,
            "country": "Oman",
            "population": 4636262,
            "sq_km_area": 309500,
            "life_expectancy": "71.8",
            "elevation_in_meters": 310,
            "continent": "Asia",
            "abbreviation": "OM",
            "location": "Middle East",
            "iso": 512,
            "capital_city": "Masqat",
            "lat": "21.512583",
            "long": "55.923255",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Pakistan": {
        "All": {
            "confirmed": 890391,
            "recovered": 804122,
            "deaths": 19987,
            "country": "Pakistan",
            "population": 197015955,
            "sq_km_area": 796095,
            "life_expectancy": "61.1",
            "elevation_in_meters": 900,
            "continent": "Asia",
            "abbreviation": "PK",
            "location": "Southern and Central Asia",
            "iso": 586,
            "capital_city": "Islamabad"
        },
        "Azad Jammu and Kashmir": {
            "lat": "34.027401",
            "long": "73.947253",
            "confirmed": 18469,
            "recovered": 16231,
            "deaths": 521,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Balochistan": {
            "lat": "28.328492",
            "long": "65.898403",
            "confirmed": 24223,
            "recovered": 22874,
            "deaths": 270,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Gilgit-Baltistan": {
            "lat": "35.792146",
            "long": "74.982138",
            "confirmed": 5452,
            "recovered": 5281,
            "deaths": 107,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Islamabad": {
            "lat": "33.665087",
            "long": "73.121219",
            "confirmed": 79789,
            "recovered": 70999,
            "deaths": 740,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Khyber Pakhtunkhwa": {
            "lat": "34.485332",
            "long": "72.09169",
            "confirmed": 128033,
            "recovered": 117176,
            "deaths": 3855,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Punjab": {
            "lat": "30.811346",
            "long": "72.139132",
            "confirmed": 331102,
            "recovered": 293281,
            "deaths": 9640,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Sindh": {
            "lat": "26.009446",
            "long": "68.776807",
            "confirmed": 303323,
            "recovered": 278280,
            "deaths": 4854,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Panama": {
        "All": {
            "confirmed": 372221,
            "recovered": 360254,
            "deaths": 6305,
            "country": "Panama",
            "population": 4098587,
            "sq_km_area": 75517,
            "life_expectancy": "75.5",
            "elevation_in_meters": 360,
            "continent": "North America",
            "abbreviation": "PA",
            "location": "Central America",
            "iso": 591,
            "capital_city": "Ciudad de Panam",
            "lat": "8.538",
            "long": "-80.7821",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Papua New Guinea": {
        "All": {
            "confirmed": 14910,
            "recovered": 13801,
            "deaths": 154,
            "country": "Papua New Guinea",
            "population": 8251162,
            "sq_km_area": 462840,
            "life_expectancy": "63.1",
            "elevation_in_meters": 667,
            "continent": "Oceania",
            "abbreviation": "PG",
            "location": "Melanesia",
            "iso": 598,
            "capital_city": "Port Moresby",
            "lat": "-6.314993",
            "long": "143.95555",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Paraguay": {
        "All": {
            "confirmed": 321032,
            "recovered": 266328,
            "deaths": 7914,
            "country": "Paraguay",
            "population": 6811297,
            "sq_km_area": 406752,
            "life_expectancy": "73.7",
            "elevation_in_meters": 178,
            "continent": "South America",
            "abbreviation": "PY",
            "location": "South America",
            "iso": 600,
            "capital_city": "Asunci",
            "lat": "-23.4425",
            "long": "-58.4438",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Peru": {
        "All": {
            "confirmed": 1903615,
            "recovered": 1847981,
            "deaths": 67034,
            "country": "Peru",
            "population": 32165485,
            "sq_km_area": 1285216,
            "life_expectancy": 70,
            "elevation_in_meters": "1,555",
            "continent": "South America",
            "abbreviation": "PE",
            "location": "South America",
            "iso": 604,
            "capital_city": "Lima"
        },
        "Amazonas": {
            "lat": "-5.077253",
            "long": "-78.050172",
            "confirmed": 27752,
            "recovered": 0,
            "deaths": 502,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ancash": {
            "lat": "-9.407125",
            "long": "-77.671795",
            "confirmed": 67214,
            "recovered": 0,
            "deaths": 2917,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Apurimac": {
            "lat": "-14.027713",
            "long": "-72.975378",
            "confirmed": 21868,
            "recovered": 0,
            "deaths": 571,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Arequipa": {
            "lat": "-15.843524",
            "long": "-72.475539",
            "confirmed": 78326,
            "recovered": 0,
            "deaths": 2717,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ayacucho": {
            "lat": "-14.091648",
            "long": "-74.08344",
            "confirmed": 27857,
            "recovered": 0,
            "deaths": 814,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cajamarca": {
            "lat": "-6.430284",
            "long": "-78.745596",
            "confirmed": 55982,
            "recovered": 0,
            "deaths": 1425,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Callao": {
            "lat": "-11.954609",
            "long": "-77.136042",
            "confirmed": 93946,
            "recovered": 0,
            "deaths": 3385,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Cusco": {
            "lat": "-13.191068",
            "long": "-72.153609",
            "confirmed": 59506,
            "recovered": 0,
            "deaths": 1351,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Huancavelica": {
            "lat": "-13.023888",
            "long": "-75.00277",
            "confirmed": 14204,
            "recovered": 0,
            "deaths": 418,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Huanuco": {
            "lat": "-9.421676",
            "long": "-76.040642",
            "confirmed": 31189,
            "recovered": 0,
            "deaths": 1042,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ica": {
            "lat": "-14.235097",
            "long": "-75.574821",
            "confirmed": 50994,
            "recovered": 0,
            "deaths": 2980,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Junin": {
            "lat": "-11.541783",
            "long": "-74.876968",
            "confirmed": 68254,
            "recovered": 0,
            "deaths": 2432,
            "updated": "2021/05/20 17:21:11+00"
        },
        "La Libertad": {
            "lat": "-7.92139",
            "long": "-78.370238",
            "confirmed": 73511,
            "recovered": 0,
            "deaths": 4050,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Lambayeque": {
            "lat": "-6.353049",
            "long": "-79.824113",
            "confirmed": 53673,
            "recovered": 0,
            "deaths": 2686,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Lima": {
            "lat": "-11.766533",
            "long": "-76.604498",
            "confirmed": 863454,
            "recovered": 0,
            "deaths": 29013,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Loreto": {
            "lat": "-4.124847",
            "long": "-74.424115",
            "confirmed": 39350,
            "recovered": 0,
            "deaths": 1410,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Madre de Dios": {
            "lat": "-11.972699",
            "long": "-70.53172",
            "confirmed": 12969,
            "recovered": 0,
            "deaths": 235,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Moquegua": {
            "lat": "-16.860271",
            "long": "-70.839046",
            "confirmed": 24034,
            "recovered": 0,
            "deaths": 572,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Pasco": {
            "lat": "-10.39655",
            "long": "-75.307635",
            "confirmed": 13724,
            "recovered": 0,
            "deaths": 418,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Piura": {
            "lat": "-5.133361",
            "long": "-80.335861",
            "confirmed": 75096,
            "recovered": 0,
            "deaths": 3230,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Puno": {
            "lat": "-14.995827",
            "long": "-69.922726",
            "confirmed": 30944,
            "recovered": 0,
            "deaths": 954,
            "updated": "2021/05/20 17:21:11+00"
        },
        "San Martin": {
            "lat": "-7.039531",
            "long": "-76.729127",
            "confirmed": 42704,
            "recovered": 0,
            "deaths": 1280,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tacna": {
            "lat": "-17.644161",
            "long": "-70.27756",
            "confirmed": 24405,
            "recovered": 0,
            "deaths": 801,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Tumbes": {
            "lat": "-3.857496",
            "long": "-80.545255",
            "confirmed": 16140,
            "recovered": 0,
            "deaths": 595,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Ucayali": {
            "lat": "-9.621718",
            "long": "-73.444929",
            "confirmed": 30804,
            "recovered": 0,
            "deaths": 972,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 5715,
            "recovered": 1847981,
            "deaths": 264,
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Philippines": {
        "All": {
            "confirmed": 1165155,
            "recovered": 1093602,
            "deaths": 19641,
            "country": "Philippines",
            "population": 104918090,
            "sq_km_area": 300000,
            "life_expectancy": "67.5",
            "elevation_in_meters": 442,
            "continent": "Asia",
            "abbreviation": "PH",
            "location": "Southeast Asia",
            "iso": 608,
            "capital_city": "Manila",
            "lat": "12.879721",
            "long": "121.774017",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Poland": {
        "All": {
            "confirmed": 2861351,
            "recovered": 2614020,
            "deaths": 72500,
            "country": "Poland",
            "population": 38170712,
            "sq_km_area": 323250,
            "life_expectancy": "73.2",
            "elevation_in_meters": 173,
            "continent": "Europe",
            "abbreviation": "PL",
            "location": "Eastern Europe",
            "iso": 616,
            "capital_city": "Warszawa",
            "lat": "51.9194",
            "long": "19.1451",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Portugal": {
        "All": {
            "confirmed": 843729,
            "recovered": 804522,
            "deaths": 17014,
            "country": "Portugal",
            "population": 10329506,
            "sq_km_area": 91982,
            "life_expectancy": "75.8",
            "elevation_in_meters": 372,
            "continent": "Europe",
            "abbreviation": "PT",
            "location": "Southern Europe",
            "iso": 620,
            "capital_city": "Lisboa",
            "lat": "39.3999",
            "long": "-8.2245",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Qatar": {
        "All": {
            "confirmed": 214463,
            "recovered": 209773,
            "deaths": 539,
            "country": "Qatar",
            "population": 2639211,
            "sq_km_area": 11000,
            "life_expectancy": "72.4",
            "elevation_in_meters": 28,
            "continent": "Asia",
            "abbreviation": "QA",
            "location": "Middle East",
            "iso": 634,
            "capital_city": "Doha",
            "lat": "25.3548",
            "long": "51.1839",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Romania": {
        "All": {
            "confirmed": 1074297,
            "recovered": 1029375,
            "deaths": 29777,
            "country": "Romania",
            "population": 19679306,
            "sq_km_area": 238391,
            "life_expectancy": "69.9",
            "elevation_in_meters": 414,
            "continent": "Europe",
            "abbreviation": "RO",
            "location": "Eastern Europe",
            "iso": 642,
            "capital_city": "Bucuresti",
            "lat": "45.9432",
            "long": "24.9668",
            "updated": "2021/05/20 17:21:11+00"
        }
    },
    "Russia": {
        "All": {
            "confirmed": 4917906,
            "recovered": 4538909,
            "deaths": 115393,
            "country": "Russia",
            "population": 143989754,
            "sq_km_area": 17075400,
            "life_expectancy": "67.2",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "RU",
            "location": "Eastern Europe",
            "iso": null,
            "capital_city": "Moscow"
        },
        "Adygea Republic": {
            "lat": "44.6939006",
            "long": "40.1520421",
            "confirmed": 14589,
            "recovered": 14041,
            "deaths": 208,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Altai Krai": {
            "lat": "52.6932243",
            "long": "82.6931424",
            "confirmed": 50972,
            "recovered": 45929,
            "deaths": 2211,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Altai Republic": {
            "lat": "50.7114101",
            "long": "86.8572186",
            "confirmed": 16910,
            "recovered": 16580,
            "deaths": 242,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Amur Oblast": {
            "lat": "52.8032368",
            "long": "128.437295",
            "confirmed": 22578,
            "recovered": 21909,
            "deaths": 290,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Arkhangelsk Oblast": {
            "lat": "63.5589686",
            "long": "43.1221646",
            "confirmed": 63051,
            "recovered": 60123,
            "deaths": 970,
            "updated": "2021/05/20 17:21:11+00"
        },
        "Astrakhan Oblast": {
            "lat": "47.1878186",
            "long": "47.608851",
            "confirmed": 33768,
            "recovered": 32259,
            "deaths": 759,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Bashkortostan Republic": {
            "lat": "54.8573563",
            "long": "57.1439682",
            "confirmed": 37218,
            "recovered": 34939,
            "deaths": 499,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Belgorod Oblast": {
            "lat": "50.7080119",
            "long": "37.5837615",
            "confirmed": 38114,
            "recovered": 35479,
            "deaths": 672,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Bryansk Oblast": {
            "lat": "52.8873315",
            "long": "33.415853",
            "confirmed": 39490,
            "recovered": 38660,
            "deaths": 325,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Buryatia Republic": {
            "lat": "52.7182426",
            "long": "109.492143",
            "confirmed": 37749,
            "recovered": 36508,
            "deaths": 883,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chechen Republic": {
            "lat": "43.3976147",
            "long": "45.6985005",
            "confirmed": 12187,
            "recovered": 11921,
            "deaths": 131,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chelyabinsk Oblast": {
            "lat": "54.4223954",
            "long": "61.1865846",
            "confirmed": 60861,
            "recovered": 56529,
            "deaths": 1558,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chukotka Autonomous Okrug": {
            "lat": "66.0006475",
            "long": "169.4900869",
            "confirmed": 763,
            "recovered": 733,
            "deaths": 7,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chuvashia Republic": {
            "lat": "55.4259922",
            "long": "47.0849429",
            "confirmed": 25238,
            "recovered": 23039,
            "deaths": 1404,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Dagestan Republic": {
            "lat": "43.0574916",
            "long": "47.1332224",
            "confirmed": 33181,
            "recovered": 30805,
            "deaths": 1479,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ingushetia Republic": {
            "lat": "43.11542075",
            "long": "45.01713552",
            "confirmed": 15654,
            "recovered": 15246,
            "deaths": 188,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Irkutsk Oblast": {
            "lat": "56.6370122",
            "long": "104.719221",
            "confirmed": 68625,
            "recovered": 65524,
            "deaths": 2396,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ivanovo Oblast": {
            "lat": "56.9167446",
            "long": "41.4352137",
            "confirmed": 35625,
            "recovered": 33516,
            "deaths": 1187,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Jewish Autonomous Okrug": {
            "lat": "48.57527615",
            "long": "132.6630746",
            "confirmed": 4484,
            "recovered": 4307,
            "deaths": 141,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kabardino-Balkarian Republic": {
            "lat": "43.4806048",
            "long": "43.5978976",
            "confirmed": 24161,
            "recovered": 23450,
            "deaths": 464,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kaliningrad Oblast": {
            "lat": "54.7293041",
            "long": "21.1489473",
            "confirmed": 32955,
            "recovered": 31677,
            "deaths": 468,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kalmykia Republic": {
            "lat": "46.2313018",
            "long": "45.3275745",
            "confirmed": 20681,
            "recovered": 20017,
            "deaths": 366,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kaluga Oblast": {
            "lat": "54.4382773",
            "long": "35.5272854",
            "confirmed": 35128,
            "recovered": 33440,
            "deaths": 329,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kamchatka Krai": {
            "lat": "57.1914882",
            "long": "160.0383819",
            "confirmed": 14607,
            "recovered": 13970,
            "deaths": 231,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Karachay-Cherkess Republic": {
            "lat": "43.7368326",
            "long": "41.7267991",
            "confirmed": 20306,
            "recovered": 19964,
            "deaths": 108,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Karelia Republic": {
            "lat": "62.6194031",
            "long": "33.4920267",
            "confirmed": 46613,
            "recovered": 44823,
            "deaths": 520,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kemerovo Oblast": {
            "lat": "54.5335781",
            "long": "87.342861",
            "confirmed": 36350,
            "recovered": 35097,
            "deaths": 703,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Khabarovsk Krai": {
            "lat": "51.6312684",
            "long": "136.121524",
            "confirmed": 52189,
            "recovered": 50146,
            "deaths": 399,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Khakassia Republic": {
            "lat": "53.72258845",
            "long": "91.44293627",
            "confirmed": 22832,
            "recovered": 21795,
            "deaths": 544,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Khanty-Mansi Autonomous Okrug": {
            "lat": "61.0259025",
            "long": "69.0982628",
            "confirmed": 56945,
            "recovered": 55725,
            "deaths": 796,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kirov Oblast": {
            "lat": "57.9665589",
            "long": "49.4074599",
            "confirmed": 43328,
            "recovered": 40798,
            "deaths": 356,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Komi Republic": {
            "lat": "63.9881421",
            "long": "54.3326073",
            "confirmed": 43803,
            "recovered": 42466,
            "deaths": 943,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kostroma Oblast": {
            "lat": "58.424756",
            "long": "44.2533273",
            "confirmed": 21166,
            "recovered": 18433,
            "deaths": 461,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Krasnodar Krai": {
            "lat": "45.7684014",
            "long": "39.0261044",
            "confirmed": 47387,
            "recovered": 42320,
            "deaths": 2898,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Krasnoyarsk Krai": {
            "lat": "63.3233807",
            "long": "97.0979974",
            "confirmed": 72454,
            "recovered": 66754,
            "deaths": 3667,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kurgan Oblast": {
            "lat": "55.7655302",
            "long": "64.5632681",
            "confirmed": 21022,
            "recovered": 19750,
            "deaths": 406,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kursk Oblast": {
            "lat": "51.6568453",
            "long": "36.4852695",
            "confirmed": 37751,
            "recovered": 35948,
            "deaths": 780,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Leningrad Oblast": {
            "lat": "60.1853296",
            "long": "32.3925325",
            "confirmed": 44247,
            "recovered": 41458,
            "deaths": 1338,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Lipetsk Oblast": {
            "lat": "52.6935178",
            "long": "39.1122664",
            "confirmed": 31040,
            "recovered": 28068,
            "deaths": 713,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Magadan Oblast": {
            "lat": "62.48858785",
            "long": "153.9903764",
            "confirmed": 8412,
            "recovered": 8213,
            "deaths": 106,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Mari El Republic": {
            "lat": "56.5767504",
            "long": "47.8817512",
            "confirmed": 13352,
            "recovered": 12915,
            "deaths": 234,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Mordovia Republic": {
            "lat": "54.4419829",
            "long": "44.4661144",
            "confirmed": 20645,
            "recovered": 18435,
            "deaths": 266,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Moscow": {
            "lat": "55.7504461",
            "long": "37.6174943",
            "confirmed": 1148508,
            "recovered": 1035739,
            "deaths": 19474,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Moscow Oblast": {
            "lat": "55.5043158",
            "long": "38.0353929",
            "confirmed": 262937,
            "recovered": 215561,
            "deaths": 6040,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Murmansk Oblast": {
            "lat": "68.0000418",
            "long": "33.9999151",
            "confirmed": 50893,
            "recovered": 48000,
            "deaths": 1196,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Nenets Autonomous Okrug": {
            "lat": "68.27557185",
            "long": "57.1686375",
            "confirmed": 1097,
            "recovered": 1088,
            "deaths": 4,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Nizhny Novgorod Oblast": {
            "lat": "55.4718033",
            "long": "44.0911594",
            "confirmed": 116527,
            "recovered": 111663,
            "deaths": 3465,
            "updated": "2021/05/20 18:21:02+00"
        },
        "North Ossetia - Alania Republic": {
            "lat": "42.7933611",
            "long": "44.6324493",
            "confirmed": 16914,
            "recovered": 16278,
            "deaths": 203,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Novgorod Oblast": {
            "lat": "58.2843833",
            "long": "32.5169757",
            "confirmed": 30590,
            "recovered": 29507,
            "deaths": 142,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Novosibirsk Oblast": {
            "lat": "54.9720169",
            "long": "79.4813924",
            "confirmed": 42532,
            "recovered": 38640,
            "deaths": 1832,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Omsk Oblast": {
            "lat": "56.0935263",
            "long": "73.5099936",
            "confirmed": 46830,
            "recovered": 44454,
            "deaths": 1418,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Orel Oblast": {
            "lat": "52.9685433",
            "long": "36.0692477",
            "confirmed": 35122,
            "recovered": 33215,
            "deaths": 639,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Orenburg Oblast": {
            "lat": "52.0269262",
            "long": "54.7276647",
            "confirmed": 43549,
            "recovered": 39803,
            "deaths": 933,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Penza Oblast": {
            "lat": "53.1655415",
            "long": "44.7879181",
            "confirmed": 46468,
            "recovered": 43831,
            "deaths": 748,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Perm Krai": {
            "lat": "58.5951603",
            "long": "56.3159546",
            "confirmed": 56398,
            "recovered": 50786,
            "deaths": 2350,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Primorsky Krai": {
            "lat": "45.0819456",
            "long": "134.726645",
            "confirmed": 44029,
            "recovered": 41352,
            "deaths": 725,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Pskov Oblast": {
            "lat": "57.5358729",
            "long": "28.8586826",
            "confirmed": 37327,
            "recovered": 31267,
            "deaths": 383,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Rostov Oblast": {
            "lat": "47.6222451",
            "long": "40.7957942",
            "confirmed": 92129,
            "recovered": 82446,
            "deaths": 4302,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ryazan Oblast": {
            "lat": "54.4226732",
            "long": "40.5705246",
            "confirmed": 28716,
            "recovered": 27205,
            "deaths": 498,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Saint Petersburg": {
            "lat": "59.9606739",
            "long": "30.1586551",
            "confirmed": 430492,
            "recovered": 394610,
            "deaths": 13921,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sakha (Yakutiya) Republic": {
            "lat": "66.941626",
            "long": "129.642371",
            "confirmed": 37278,
            "recovered": 33216,
            "deaths": 646,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sakhalin Oblast": {
            "lat": "49.7219665",
            "long": "143.448533",
            "confirmed": 22778,
            "recovered": 22372,
            "deaths": 46,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Samara Oblast": {
            "lat": "53.2128813",
            "long": "50.8914633",
            "confirmed": 63789,
            "recovered": 60859,
            "deaths": 1782,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Saratov Oblast": {
            "lat": "51.6520555",
            "long": "46.8631952",
            "confirmed": 61451,
            "recovered": 57142,
            "deaths": 1020,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Smolensk Oblast": {
            "lat": "55.0343496",
            "long": "33.0192065",
            "confirmed": 30413,
            "recovered": 28656,
            "deaths": 968,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Stavropol Krai": {
            "lat": "44.8632577",
            "long": "43.4406913",
            "confirmed": 54145,
            "recovered": 51280,
            "deaths": 1454,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sverdlovsk Oblast": {
            "lat": "58.6414755",
            "long": "61.8021546",
            "confirmed": 88215,
            "recovered": 82064,
            "deaths": 3302,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tambov Oblast": {
            "lat": "52.9019574",
            "long": "41.3578918",
            "confirmed": 32307,
            "recovered": 29974,
            "deaths": 513,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tatarstan Republic": {
            "lat": "55.7648572",
            "long": "52.43104273",
            "confirmed": 21140,
            "recovered": 18080,
            "deaths": 483,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tomsk Oblast": {
            "lat": "58.6124279",
            "long": "82.0475315",
            "confirmed": 32592,
            "recovered": 31502,
            "deaths": 422,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tula Oblast": {
            "lat": "53.9570701",
            "long": "37.3690909",
            "confirmed": 39539,
            "recovered": 37082,
            "deaths": 1976,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tver Oblast": {
            "lat": "57.1134475",
            "long": "35.1744428",
            "confirmed": 41330,
            "recovered": 38815,
            "deaths": 977,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tyumen Oblast": {
            "lat": "58.8206488",
            "long": "70.3658837",
            "confirmed": 36164,
            "recovered": 34559,
            "deaths": 526,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tyva Republic": {
            "lat": "51.4017149",
            "long": "93.8582593",
            "confirmed": 15902,
            "recovered": 15675,
            "deaths": 203,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Udmurt Republic": {
            "lat": "57.1961165",
            "long": "52.6959832",
            "confirmed": 33620,
            "recovered": 30506,
            "deaths": 920,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ulyanovsk Oblast": {
            "lat": "54.1463177",
            "long": "47.2324921",
            "confirmed": 55026,
            "recovered": 52393,
            "deaths": 1312,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vladimir Oblast": {
            "lat": "56.0503336",
            "long": "40.6561633",
            "confirmed": 32998,
            "recovered": 30606,
            "deaths": 1164,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Volgograd Oblast": {
            "lat": "49.6048339",
            "long": "44.2903582",
            "confirmed": 58792,
            "recovered": 56085,
            "deaths": 1226,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vologda Oblast": {
            "lat": "60.0391461",
            "long": "43.1215213",
            "confirmed": 45641,
            "recovered": 42587,
            "deaths": 1070,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Voronezh Oblast": {
            "lat": "50.9800393",
            "long": "40.1506507",
            "confirmed": 83359,
            "recovered": 78806,
            "deaths": 2808,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Yamalo-Nenets Autonomous Okrug": {
            "lat": "67.1471631",
            "long": "74.3415488",
            "confirmed": 38885,
            "recovered": 37691,
            "deaths": 417,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Yaroslavl Oblast": {
            "lat": "57.7781976",
            "long": "39.0021095",
            "confirmed": 40204,
            "recovered": 38381,
            "deaths": 583,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Zabaykalsky Krai": {
            "lat": "52.248521",
            "long": "115.956325",
            "confirmed": 42849,
            "recovered": 41424,
            "deaths": 656,
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Rwanda": {
        "All": {
            "confirmed": 26424,
            "recovered": 24808,
            "deaths": 348,
            "country": "Rwanda",
            "population": 12208407,
            "sq_km_area": 26338,
            "life_expectancy": "39.3",
            "elevation_in_meters": "1,598",
            "continent": "Africa",
            "abbreviation": "RW",
            "location": "Eastern Africa",
            "iso": 646,
            "capital_city": "Kigali",
            "lat": "-1.9403",
            "long": "29.8739",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Saint Kitts and Nevis": {
        "All": {
            "confirmed": 46,
            "recovered": 45,
            "deaths": 0,
            "country": "Saint Kitts and Nevis",
            "population": 55345,
            "sq_km_area": 261,
            "life_expectancy": "70.7",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "KN",
            "location": "Caribbean",
            "iso": 659,
            "capital_city": "Basseterre",
            "lat": "17.357822",
            "long": "-62.782998",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Saint Lucia": {
        "All": {
            "confirmed": 4906,
            "recovered": 4581,
            "deaths": 77,
            "country": "Saint Lucia",
            "population": 178844,
            "sq_km_area": 622,
            "life_expectancy": "72.3",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "LC",
            "location": "Caribbean",
            "iso": 662,
            "capital_city": "Castries",
            "lat": "13.9094",
            "long": "-60.9789",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Saint Vincent and the Grenadines": {
        "All": {
            "confirmed": 1954,
            "recovered": 1786,
            "deaths": 12,
            "country": "Saint Vincent and the Grenadines",
            "population": 109897,
            "sq_km_area": 388,
            "life_expectancy": "72.3",
            "elevation_in_meters": null,
            "continent": "North America",
            "abbreviation": "VC",
            "location": "Caribbean",
            "iso": 670,
            "capital_city": "Kingstown",
            "lat": "12.9843",
            "long": "-61.2872",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Samoa": {
        "All": {
            "confirmed": 3,
            "recovered": 3,
            "deaths": 0,
            "country": "Samoa",
            "population": 196440,
            "sq_km_area": 2831,
            "life_expectancy": "69.2",
            "elevation_in_meters": null,
            "continent": "Oceania",
            "abbreviation": "WS",
            "location": "Polynesia",
            "iso": 882,
            "capital_city": "Apia",
            "lat": "-13.759",
            "long": "-172.1046",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "San Marino": {
        "All": {
            "confirmed": 5087,
            "recovered": 4974,
            "deaths": 90,
            "country": "San Marino",
            "population": 33400,
            "sq_km_area": 61,
            "life_expectancy": "81.1",
            "elevation_in_meters": null,
            "continent": "Europe",
            "abbreviation": "SM",
            "location": "Southern Europe",
            "iso": 674,
            "capital_city": "San Marino",
            "lat": "43.9424",
            "long": "12.4578",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Sao Tome and Principe": {
        "All": {
            "confirmed": 2334,
            "recovered": 2278,
            "deaths": 35,
            "country": "Sao Tome and Principe",
            "population": 204327,
            "sq_km_area": 964,
            "life_expectancy": "65.3",
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "ST",
            "location": "Central Africa",
            "iso": 678,
            "capital_city": "S",
            "lat": "0.1864",
            "long": "6.6131",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Saudi Arabia": {
        "All": {
            "confirmed": 437569,
            "recovered": 421726,
            "deaths": 7214,
            "country": "Saudi Arabia",
            "population": 32938213,
            "sq_km_area": 2149690,
            "life_expectancy": "67.8",
            "elevation_in_meters": 665,
            "continent": "Asia",
            "abbreviation": "SA",
            "location": "Middle East",
            "iso": 682,
            "capital_city": "Riyadh",
            "lat": "23.885942",
            "long": "45.079162",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Senegal": {
        "All": {
            "confirmed": 40961,
            "recovered": 39672,
            "deaths": 1128,
            "country": "Senegal",
            "population": 15850567,
            "sq_km_area": 196722,
            "life_expectancy": "62.2",
            "elevation_in_meters": 69,
            "continent": "Africa",
            "abbreviation": "SN",
            "location": "Western Africa",
            "iso": 686,
            "capital_city": "Dakar",
            "lat": "14.4974",
            "long": "-14.4524",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Serbia": {
        "All": {
            "confirmed": 708878,
            "recovered": 0,
            "deaths": 6739,
            "lat": "44.0165",
            "long": "21.0059",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Seychelles": {
        "All": {
            "confirmed": 9764,
            "recovered": 7826,
            "deaths": 35,
            "country": "Seychelles",
            "population": 94737,
            "sq_km_area": 455,
            "life_expectancy": "70.4",
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "SC",
            "location": "Eastern Africa",
            "iso": 690,
            "capital_city": "Victoria",
            "lat": "-4.6796",
            "long": "55.492",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Sierra Leone": {
        "All": {
            "confirmed": 4107,
            "recovered": 3104,
            "deaths": 79,
            "country": "Sierra Leone",
            "population": 7557212,
            "sq_km_area": 71740,
            "life_expectancy": "45.3",
            "elevation_in_meters": 279,
            "continent": "Africa",
            "abbreviation": "SL",
            "location": "Western Africa",
            "iso": 694,
            "capital_city": "Freetown",
            "lat": "8.460555",
            "long": "-11.779889",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Singapore": {
        "All": {
            "confirmed": 61730,
            "recovered": 61229,
            "deaths": 32,
            "country": "Singapore",
            "population": 5708844,
            "sq_km_area": 618,
            "life_expectancy": "80.1",
            "elevation_in_meters": null,
            "continent": "Asia",
            "abbreviation": "SG",
            "location": "Southeast Asia",
            "iso": 702,
            "capital_city": "Singapore",
            "lat": "1.2833",
            "long": "103.8333",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Slovakia": {
        "All": {
            "confirmed": 388391,
            "recovered": 255300,
            "deaths": 12272,
            "country": "Slovakia",
            "population": 5447662,
            "sq_km_area": 49012,
            "life_expectancy": "73.7",
            "elevation_in_meters": 458,
            "continent": "Europe",
            "abbreviation": "SK",
            "location": "Eastern Europe",
            "iso": 703,
            "capital_city": "Bratislava",
            "lat": "48.669",
            "long": "19.699",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Slovenia": {
        "All": {
            "confirmed": 250811,
            "recovered": 240211,
            "deaths": 4345,
            "country": "Slovenia",
            "population": 2079976,
            "sq_km_area": 20256,
            "life_expectancy": "74.9",
            "elevation_in_meters": 492,
            "continent": "Europe",
            "abbreviation": "SI",
            "location": "Southern Europe",
            "iso": 705,
            "capital_city": "Ljubljana",
            "lat": "46.1512",
            "long": "14.9955",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Solomon Islands": {
        "All": {
            "confirmed": 20,
            "recovered": 20,
            "deaths": 0,
            "country": "Solomon Islands",
            "population": 611343,
            "sq_km_area": 28896,
            "life_expectancy": "71.3",
            "elevation_in_meters": null,
            "continent": "Oceania",
            "abbreviation": "SB",
            "location": "Melanesia",
            "iso": 90,
            "capital_city": "Honiara",
            "lat": "-9.6457",
            "long": "160.1562",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Somalia": {
        "All": {
            "confirmed": 14575,
            "recovered": 6579,
            "deaths": 762,
            "country": "Somalia",
            "population": 14742523,
            "sq_km_area": 637657,
            "life_expectancy": "46.2",
            "elevation_in_meters": 410,
            "continent": "Africa",
            "abbreviation": "SO",
            "location": "Eastern Africa",
            "iso": 706,
            "capital_city": "Mogadishu",
            "lat": "5.152149",
            "long": "46.199616",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "South Africa": {
        "All": {
            "confirmed": 1621362,
            "recovered": 1528868,
            "deaths": 55507,
            "country": "South Africa",
            "population": 56717156,
            "sq_km_area": 1221037,
            "life_expectancy": "51.1",
            "elevation_in_meters": "1,034",
            "continent": "Africa",
            "abbreviation": "ZA",
            "location": "Southern Africa",
            "iso": 710,
            "capital_city": "Pretoria",
            "lat": "-30.5595",
            "long": "22.9375",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "South Sudan": {
        "All": {
            "confirmed": 10652,
            "recovered": 10462,
            "deaths": 115,
            "country": "South Sudan",
            "population": 12575714,
            "sq_km_area": 619745,
            "life_expectancy": null,
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "SS",
            "location": "Eastern Africa",
            "iso": 728,
            "capital_city": "Juba",
            "lat": "6.877",
            "long": "31.307",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Spain": {
        "All": {
            "confirmed": 3631467,
            "recovered": 150376,
            "deaths": 79607,
            "country": "Spain",
            "population": 46354321,
            "sq_km_area": 505992,
            "life_expectancy": "78.8",
            "elevation_in_meters": 660,
            "continent": "Europe",
            "abbreviation": "ES",
            "location": "Southern Europe",
            "iso": 724,
            "capital_city": "Madrid"
        },
        "Andalusia": {
            "lat": "37.5443",
            "long": "-4.7278",
            "confirmed": 572825,
            "recovered": 10671,
            "deaths": 9853,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Aragon": {
            "lat": "41.5976",
            "long": "-0.9057",
            "confirmed": 122692,
            "recovered": 3772,
            "deaths": 3496,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Asturias": {
            "lat": "43.3614",
            "long": "-5.8593",
            "confirmed": 51820,
            "recovered": 1063,
            "deaths": 1962,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Baleares": {
            "lat": "39.710358",
            "long": "2.995148",
            "confirmed": 60098,
            "recovered": 1533,
            "deaths": 837,
            "updated": "2021/05/20 18:21:02+00"
        },
        "C. Valenciana": {
            "lat": "39.484",
            "long": "-0.7533",
            "confirmed": 393249,
            "recovered": 9970,
            "deaths": 7373,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Canarias": {
            "lat": "28.2916",
            "long": "-16.6291",
            "confirmed": 55294,
            "recovered": 1537,
            "deaths": 766,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Cantabria": {
            "lat": "43.1828",
            "long": "-3.9878",
            "confirmed": 29877,
            "recovered": 2287,
            "deaths": 561,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Castilla - La Mancha": {
            "lat": "39.2796",
            "long": "-3.0977",
            "confirmed": 189827,
            "recovered": 6392,
            "deaths": 5900,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Castilla y Leon": {
            "lat": "41.8357",
            "long": "-4.3976",
            "confirmed": 228162,
            "recovered": 8716,
            "deaths": 6831,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Catalonia": {
            "lat": "41.5912",
            "long": "1.5209",
            "confirmed": 605100,
            "recovered": 26203,
            "deaths": 14497,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ceuta": {
            "lat": "35.8894",
            "long": "-5.3213",
            "confirmed": 5789,
            "recovered": 163,
            "deaths": 114,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Extremadura": {
            "lat": "39.4937",
            "long": "-6.0679",
            "confirmed": 75237,
            "recovered": 2652,
            "deaths": 1798,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Galicia": {
            "lat": "42.5751",
            "long": "-8.1339",
            "confirmed": 125377,
            "recovered": 9204,
            "deaths": 2396,
            "updated": "2021/05/20 18:21:02+00"
        },
        "La Rioja": {
            "lat": "42.2871",
            "long": "-2.5396",
            "confirmed": 30431,
            "recovered": 3107,
            "deaths": 769,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Madrid": {
            "lat": "40.4168",
            "long": "-3.7038",
            "confirmed": 707480,
            "recovered": 40736,
            "deaths": 15199,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Melilla": {
            "lat": "35.2923",
            "long": "-2.9381",
            "confirmed": 9004,
            "recovered": 125,
            "deaths": 97,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Murcia": {
            "lat": "37.9922",
            "long": "-1.1307",
            "confirmed": 112207,
            "recovered": 2180,
            "deaths": 1593,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Navarra": {
            "lat": "42.6954",
            "long": "-1.6761",
            "confirmed": 61740,
            "recovered": 3905,
            "deaths": 1170,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Pais Vasco": {
            "lat": "42.9896",
            "long": "-2.6189",
            "confirmed": 195258,
            "recovered": 16160,
            "deaths": 4395,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/04/25 23:20:49+00"
        }
    },
    "Sri Lanka": {
        "All": {
            "confirmed": 154786,
            "recovered": 123532,
            "deaths": 1051,
            "country": "Sri Lanka",
            "population": 20876917,
            "life_expectancy": null,
            "elevation_in_meters": null,
            "location": null,
            "iso": null,
            "capital_city": null,
            "lat": "7.873054",
            "long": "80.771797",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Sudan": {
        "All": {
            "confirmed": 34889,
            "recovered": 27949,
            "deaths": 2446,
            "country": "Sudan",
            "population": 40533330,
            "sq_km_area": 1886068,
            "life_expectancy": "56.6",
            "elevation_in_meters": 568,
            "continent": "Africa",
            "abbreviation": "SD",
            "location": "Northern Africa",
            "iso": 729,
            "capital_city": "Khartum",
            "lat": "12.8628",
            "long": "30.2176",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Suriname": {
        "All": {
            "confirmed": 12409,
            "recovered": 10507,
            "deaths": 239,
            "country": "Suriname",
            "population": 563402,
            "sq_km_area": 163265,
            "life_expectancy": "71.4",
            "elevation_in_meters": 246,
            "continent": "South America",
            "abbreviation": "SR",
            "location": "South America",
            "iso": 740,
            "capital_city": "Paramaribo",
            "lat": "3.9193",
            "long": "-56.0278",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Sweden": {
        "All": {
            "confirmed": 1055173,
            "recovered": 0,
            "deaths": 14351,
            "country": "Sweden",
            "population": 9910701,
            "sq_km_area": 449964,
            "life_expectancy": "79.6",
            "elevation_in_meters": 320,
            "continent": "Europe",
            "abbreviation": "SE",
            "location": "Nordic Countries",
            "iso": 752,
            "capital_city": "Stockholm"
        },
        "Blekinge": {
            "lat": "56.2784",
            "long": "15.018",
            "confirmed": 12357,
            "recovered": 0,
            "deaths": 128,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Dalarna": {
            "lat": "61.0917",
            "long": "14.6664",
            "confirmed": 26146,
            "recovered": 0,
            "deaths": 337,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Gavleborg": {
            "lat": "61.3012",
            "long": "16.1534",
            "confirmed": 35168,
            "recovered": 0,
            "deaths": 551,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Gotland": {
            "lat": "57.4684",
            "long": "18.4867",
            "confirmed": 3886,
            "recovered": 0,
            "deaths": 51,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Halland": {
            "lat": "56.8967",
            "long": "12.8034",
            "confirmed": 39794,
            "recovered": 0,
            "deaths": 315,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Jamtland Harjedalen": {
            "lat": "63.1712",
            "long": "14.9592",
            "confirmed": 12124,
            "recovered": 0,
            "deaths": 120,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Jonkoping": {
            "lat": "57.3708",
            "long": "14.3439",
            "confirmed": 41933,
            "recovered": 0,
            "deaths": 552,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kalmar": {
            "lat": "57.235",
            "long": "16.1849",
            "confirmed": 23339,
            "recovered": 0,
            "deaths": 243,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kronoberg": {
            "lat": "56.7183",
            "long": "14.4115",
            "confirmed": 21349,
            "recovered": 0,
            "deaths": 312,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Norrbotten": {
            "lat": "66.8309",
            "long": "20.3992",
            "confirmed": 23699,
            "recovered": 0,
            "deaths": 261,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Orebro": {
            "lat": "59.535",
            "long": "15.0066",
            "confirmed": 31132,
            "recovered": 0,
            "deaths": 331,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ostergotland": {
            "lat": "58.3454",
            "long": "15.5198",
            "confirmed": 41030,
            "recovered": 0,
            "deaths": 617,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Skane": {
            "lat": "55.9903",
            "long": "13.5958",
            "confirmed": 151892,
            "recovered": 0,
            "deaths": 1716,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sormland": {
            "lat": "59.0336",
            "long": "16.7519",
            "confirmed": 25044,
            "recovered": 0,
            "deaths": 453,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Stockholm": {
            "lat": "59.6025",
            "long": "18.1384",
            "confirmed": 249011,
            "recovered": 0,
            "deaths": 4294,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Uppsala": {
            "lat": "60.0092",
            "long": "17.2715",
            "confirmed": 39328,
            "recovered": 0,
            "deaths": 533,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Varmland": {
            "lat": "59.7294",
            "long": "13.2354",
            "confirmed": 18212,
            "recovered": 0,
            "deaths": 209,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vasterbotten": {
            "lat": "65.3337",
            "long": "16.5162",
            "confirmed": 22560,
            "recovered": 0,
            "deaths": 180,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vasternorrland": {
            "lat": "63.4276",
            "long": "17.7292",
            "confirmed": 26109,
            "recovered": 0,
            "deaths": 466,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vastmanland": {
            "lat": "59.6714",
            "long": "16.2159",
            "confirmed": 26799,
            "recovered": 0,
            "deaths": 345,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vastra Gotaland": {
            "lat": "58.2528",
            "long": "13.0596",
            "confirmed": 184261,
            "recovered": 0,
            "deaths": 2337,
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Switzerland": {
        "All": {
            "confirmed": 686152,
            "recovered": 317600,
            "deaths": 10760,
            "country": "Switzerland",
            "population": 8476005,
            "sq_km_area": 41284,
            "life_expectancy": "79.6",
            "elevation_in_meters": "1,350",
            "continent": "Europe",
            "abbreviation": "CH",
            "location": "Western Europe",
            "iso": 756,
            "capital_city": "Bern",
            "lat": "46.8182",
            "long": "8.2275",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Syria": {
        "All": {
            "confirmed": 23884,
            "recovered": 21460,
            "deaths": 1714,
            "country": "Syria",
            "population": 18269868,
            "sq_km_area": 185180,
            "life_expectancy": "68.5",
            "elevation_in_meters": 514,
            "continent": "Asia",
            "abbreviation": "SY",
            "location": "Middle East",
            "iso": 760,
            "capital_city": "Damascus",
            "lat": "34.802075",
            "long": "38.996815",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Taiwan*": {
        "All": {
            "confirmed": 2825,
            "recovered": 1133,
            "deaths": 15,
            "lat": "23.7",
            "long": "121",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Tajikistan": {
        "All": {
            "confirmed": 13308,
            "recovered": 13218,
            "deaths": 90,
            "country": "Tajikistan",
            "population": 8921343,
            "sq_km_area": 143100,
            "life_expectancy": "64.1",
            "elevation_in_meters": "3,186",
            "continent": "Asia",
            "abbreviation": "TJ",
            "location": "Southern and Central Asia",
            "iso": 762,
            "capital_city": "Dushanbe",
            "lat": "38.861",
            "long": "71.2761",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Tanzania": {
        "All": {
            "confirmed": 509,
            "recovered": 183,
            "deaths": 21,
            "country": "Tanzania",
            "population": 57310019,
            "sq_km_area": 883749,
            "life_expectancy": "52.3",
            "elevation_in_meters": "1,018",
            "continent": "Africa",
            "abbreviation": "TZ",
            "location": "Eastern Africa",
            "iso": 834,
            "capital_city": "Dodoma",
            "lat": "-6.369028",
            "long": "34.888822",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Thailand": {
        "All": {
            "confirmed": 119585,
            "recovered": 26873,
            "deaths": 703,
            "country": "Thailand",
            "population": 69037513,
            "sq_km_area": 513115,
            "life_expectancy": "68.6",
            "elevation_in_meters": 287,
            "continent": "Asia",
            "abbreviation": "TH",
            "location": "Southeast Asia",
            "iso": 764,
            "capital_city": "Bangkok",
            "lat": "15.870032",
            "long": "100.992541",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Timor-Leste": {
        "All": {
            "confirmed": 5121,
            "recovered": 2716,
            "deaths": 11,
            "lat": "-8.874217",
            "long": "125.727539",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Togo": {
        "All": {
            "confirmed": 13299,
            "recovered": 12013,
            "deaths": 125,
            "country": "Togo",
            "population": 7797694,
            "sq_km_area": 56785,
            "life_expectancy": "54.7",
            "elevation_in_meters": 236,
            "continent": "Africa",
            "abbreviation": "TG",
            "location": "Western Africa",
            "iso": 768,
            "capital_city": "Lom",
            "lat": "8.6195",
            "long": "0.8248",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Trinidad and Tobago": {
        "All": {
            "confirmed": 17669,
            "recovered": 10689,
            "deaths": 331,
            "country": "Trinidad and Tobago",
            "population": 1369125,
            "sq_km_area": 5130,
            "life_expectancy": 68,
            "elevation_in_meters": 83,
            "continent": "North America",
            "abbreviation": "TT",
            "location": "Caribbean",
            "iso": 780,
            "capital_city": "Port-of-Spain",
            "lat": "10.6918",
            "long": "-61.2225",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Tunisia": {
        "All": {
            "confirmed": 329925,
            "recovered": 291515,
            "deaths": 12032,
            "country": "Tunisia",
            "population": 11532127,
            "sq_km_area": 163610,
            "life_expectancy": "73.7",
            "elevation_in_meters": 246,
            "continent": "Africa",
            "abbreviation": "TN",
            "location": "Northern Africa",
            "iso": 788,
            "capital_city": "Tunis",
            "lat": "33.886917",
            "long": "9.537499",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Turkey": {
        "All": {
            "confirmed": 5160423,
            "recovered": 4989787,
            "deaths": 45626,
            "country": "Turkey",
            "population": 80745020,
            "sq_km_area": 774815,
            "life_expectancy": 71,
            "elevation_in_meters": "1,132",
            "continent": "Asia",
            "abbreviation": "TR",
            "location": "Middle East",
            "iso": 792,
            "capital_city": "Ankara",
            "lat": "38.9637",
            "long": "35.2433",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Uganda": {
        "All": {
            "confirmed": 43094,
            "recovered": 42429,
            "deaths": 350,
            "country": "Uganda",
            "population": 42862958,
            "sq_km_area": 241038,
            "life_expectancy": "42.9",
            "elevation_in_meters": null,
            "continent": "Africa",
            "abbreviation": "UG",
            "location": "Eastern Africa",
            "iso": 800,
            "capital_city": "Kampala",
            "lat": "1.373333",
            "long": "32.290275",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Ukraine": {
        "All": {
            "confirmed": 2227400,
            "recovered": 1969055,
            "deaths": 50867,
            "country": "Ukraine",
            "population": 44222947,
            "sq_km_area": 603700,
            "life_expectancy": 66,
            "elevation_in_meters": 175,
            "continent": "Europe",
            "abbreviation": "UA",
            "location": "Eastern Europe",
            "iso": 804,
            "capital_city": "Kyiv"
        },
        "Cherkasy Oblast": {
            "lat": "49.4444",
            "long": "32.0598",
            "confirmed": 79568,
            "recovered": 69961,
            "deaths": 1191,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chernihiv Oblast": {
            "lat": "51.4982",
            "long": "31.2893",
            "confirmed": 55871,
            "recovered": 51228,
            "deaths": 1270,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Chernivtsi Oblast": {
            "lat": "48.2917",
            "long": "25.9352",
            "confirmed": 78654,
            "recovered": 72657,
            "deaths": 1730,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Crimea Republic*": {
            "lat": "45.2835",
            "long": "34.2008",
            "confirmed": 42459,
            "recovered": 39392,
            "deaths": 1303,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Dnipropetrovsk Oblast": {
            "lat": "48.4647",
            "long": "35.0462",
            "confirmed": 130367,
            "recovered": 115333,
            "deaths": 3879,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Donetsk Oblast": {
            "lat": "48.0159",
            "long": "37.8028",
            "confirmed": 87476,
            "recovered": 76838,
            "deaths": 2106,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ivano-Frankivsk Oblast": {
            "lat": "48.9226",
            "long": "24.7111",
            "confirmed": 85598,
            "recovered": 81634,
            "deaths": 2037,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kharkiv Oblast": {
            "lat": "49.9935",
            "long": "36.2304",
            "confirmed": 144571,
            "recovered": 125934,
            "deaths": 3115,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kherson Oblast": {
            "lat": "46.6354",
            "long": "32.6169",
            "confirmed": 33758,
            "recovered": 28710,
            "deaths": 1014,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Khmelnytskyi Oblast": {
            "lat": "49.423",
            "long": "26.9871",
            "confirmed": 87032,
            "recovered": 79838,
            "deaths": 1763,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kiev": {
            "lat": "50.4501",
            "long": "30.5234",
            "confirmed": 203327,
            "recovered": 137960,
            "deaths": 4983,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kiev Oblast": {
            "lat": "50.053",
            "long": "30.7667",
            "confirmed": 123102,
            "recovered": 114234,
            "deaths": 2618,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kirovohrad Oblast": {
            "lat": "48.5079",
            "long": "32.2623",
            "confirmed": 19898,
            "recovered": 14917,
            "deaths": 763,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Luhansk Oblast": {
            "lat": "48.574",
            "long": "39.3078",
            "confirmed": 25630,
            "recovered": 22478,
            "deaths": 843,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Lviv Oblast": {
            "lat": "49.8397",
            "long": "24.0297",
            "confirmed": 133944,
            "recovered": 122013,
            "deaths": 3503,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Mykolaiv Oblast": {
            "lat": "46.975",
            "long": "31.9946",
            "confirmed": 66924,
            "recovered": 62661,
            "deaths": 1760,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Odessa Oblast": {
            "lat": "46.4846",
            "long": "30.7326",
            "confirmed": 138341,
            "recovered": 116104,
            "deaths": 2836,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Poltava Oblast": {
            "lat": "49.5883",
            "long": "34.5514",
            "confirmed": 75992,
            "recovered": 70969,
            "deaths": 1727,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Rivne Oblast": {
            "lat": "50.6199",
            "long": "26.2516",
            "confirmed": 76665,
            "recovered": 72848,
            "deaths": 1143,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sevastopol*": {
            "lat": "44.6054",
            "long": "33.522",
            "confirmed": 14543,
            "recovered": 13469,
            "deaths": 665,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Sumy Oblast": {
            "lat": "50.9077",
            "long": "34.7981",
            "confirmed": 75967,
            "recovered": 71658,
            "deaths": 1254,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ternopil Oblast": {
            "lat": "49.5535",
            "long": "25.5948",
            "confirmed": 68703,
            "recovered": 63608,
            "deaths": 1151,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vinnytsia Oblast": {
            "lat": "49.2331",
            "long": "28.4682",
            "confirmed": 69812,
            "recovered": 60264,
            "deaths": 1628,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Volyn Oblast": {
            "lat": "50.7472",
            "long": "25.3254",
            "confirmed": 60269,
            "recovered": 55309,
            "deaths": 1111,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Zakarpattia Oblast": {
            "lat": "48.6208",
            "long": "22.2879",
            "confirmed": 61180,
            "recovered": 57941,
            "deaths": 1572,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Zaporizhia Oblast": {
            "lat": "47.8388",
            "long": "35.1396",
            "confirmed": 100931,
            "recovered": 91025,
            "deaths": 2200,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Zhytomyr Oblast": {
            "lat": "50.2547",
            "long": "28.6587",
            "confirmed": 86818,
            "recovered": 80072,
            "deaths": 1702,
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "United Arab Emirates": {
        "All": {
            "confirmed": 551430,
            "recovered": 531459,
            "deaths": 1642,
            "country": "United Arab Emirates",
            "population": 9400145,
            "sq_km_area": 83600,
            "life_expectancy": "74.1",
            "elevation_in_meters": 149,
            "continent": "Asia",
            "abbreviation": "AE",
            "location": "Middle East",
            "iso": 784,
            "capital_city": "Abu Dhabi",
            "lat": "23.424076",
            "long": "53.847818",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "United Kingdom": {
        "All": {
            "confirmed": 4471061,
            "recovered": 15352,
            "deaths": 127963,
            "country": "United Kingdom",
            "population": 66181585,
            "sq_km_area": 242900,
            "life_expectancy": "77.7",
            "elevation_in_meters": 162,
            "continent": "Europe",
            "abbreviation": "GB",
            "location": "British Isles",
            "iso": 826,
            "capital_city": "London"
        },
        "Anguilla": {
            "lat": "18.2206",
            "long": "-63.0686",
            "confirmed": 109,
            "recovered": 107,
            "deaths": 0,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Bermuda": {
            "lat": "32.3078",
            "long": "-64.7505",
            "confirmed": 2479,
            "recovered": 2311,
            "deaths": 32,
            "updated": "2021/05/20 18:21:02+00"
        },
        "British Virgin Islands": {
            "lat": "18.4207",
            "long": "-64.64",
            "confirmed": 248,
            "recovered": 209,
            "deaths": 1,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Cayman Islands": {
            "lat": "19.3133",
            "long": "-81.2546",
            "confirmed": 574,
            "recovered": 554,
            "deaths": 2,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Channel Islands": {
            "lat": "49.3723",
            "long": "-2.3644",
            "confirmed": 4059,
            "recovered": 3956,
            "deaths": 86,
            "updated": "2021/05/20 18:21:02+00"
        },
        "England": {
            "lat": "52.3555",
            "long": "-1.1743",
            "confirmed": 3890081,
            "recovered": 0,
            "deaths": 112324,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Falkland Islands (Malvinas)": {
            "lat": "-51.7963",
            "long": "-59.5236",
            "confirmed": 63,
            "recovered": 63,
            "deaths": 0,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Gibraltar": {
            "lat": "36.1408",
            "long": "-5.3536",
            "confirmed": 4286,
            "recovered": 4192,
            "deaths": 94,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Isle of Man": {
            "lat": "54.2361",
            "long": "-4.5481",
            "confirmed": 1591,
            "recovered": 1558,
            "deaths": 29,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Montserrat": {
            "lat": "16.742498",
            "long": "-62.187366",
            "confirmed": 20,
            "recovered": 19,
            "deaths": 1,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Northern Ireland": {
            "lat": "54.7877",
            "long": "-6.4923",
            "confirmed": 121911,
            "recovered": 0,
            "deaths": 2152,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Saint Helena, Ascension and Tristan da Cunha": {
            "lat": "-7.9467",
            "long": "-14.3559",
            "confirmed": 4,
            "recovered": 4,
            "deaths": 0,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Scotland": {
            "lat": "56.4907",
            "long": "-4.2026",
            "confirmed": 230868,
            "recovered": 0,
            "deaths": 7664,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Turks and Caicos Islands": {
            "lat": "21.694",
            "long": "-71.7979",
            "confirmed": 2407,
            "recovered": 2379,
            "deaths": 17,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Unknown": {
            "lat": "",
            "long": "",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Wales": {
            "lat": "52.1307",
            "long": "-3.7837",
            "confirmed": 212361,
            "recovered": 0,
            "deaths": 5561,
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Uruguay": {
        "All": {
            "confirmed": 249365,
            "recovered": 216589,
            "deaths": 3638,
            "country": "Uruguay",
            "population": 3456750,
            "sq_km_area": 175016,
            "life_expectancy": "75.2",
            "elevation_in_meters": 109,
            "continent": "South America",
            "abbreviation": "UY",
            "location": "South America",
            "iso": 858,
            "capital_city": "Montevideo",
            "lat": "-32.5228",
            "long": "-55.7658",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Uzbekistan": {
        "All": {
            "confirmed": 97543,
            "recovered": 93071,
            "deaths": 676,
            "country": "Uzbekistan",
            "population": 31910641,
            "sq_km_area": 447400,
            "life_expectancy": "63.7",
            "elevation_in_meters": null,
            "continent": "Asia",
            "abbreviation": "UZ",
            "location": "Southern and Central Asia",
            "iso": 860,
            "capital_city": "Toskent",
            "lat": "41.377491",
            "long": "64.585262",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Vanuatu": {
        "All": {
            "confirmed": 4,
            "recovered": 3,
            "deaths": 1,
            "country": "Vanuatu",
            "population": 276244,
            "sq_km_area": 12189,
            "life_expectancy": "60.6",
            "elevation_in_meters": null,
            "continent": "Oceania",
            "abbreviation": "VU",
            "location": "Melanesia",
            "iso": 548,
            "capital_city": "Port-Vila",
            "lat": "-15.3767",
            "long": "166.9592",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Venezuela": {
        "All": {
            "confirmed": 218814,
            "recovered": 202625,
            "deaths": 2446,
            "country": "Venezuela",
            "population": 31977065,
            "sq_km_area": 912050,
            "life_expectancy": "73.1",
            "elevation_in_meters": 450,
            "continent": "South America",
            "abbreviation": "VE",
            "location": "South America",
            "iso": 862,
            "capital_city": "Caracas",
            "lat": "6.4238",
            "long": "-66.5897",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Vietnam": {
        "All": {
            "confirmed": 4809,
            "recovered": 2687,
            "deaths": 39,
            "country": "Vietnam",
            "population": 95540800,
            "sq_km_area": 331689,
            "life_expectancy": "69.3",
            "elevation_in_meters": 398,
            "continent": "Asia",
            "abbreviation": "VN",
            "location": "Southeast Asia",
            "iso": 704,
            "capital_city": "Hanoi",
            "lat": "14.058324",
            "long": "108.277199",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "West Bank and Gaza": {
        "All": {
            "confirmed": 304532,
            "recovered": 295643,
            "deaths": 3448,
            "lat": "31.9522",
            "long": "35.2332",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Yemen": {
        "All": {
            "confirmed": 6613,
            "recovered": 3159,
            "deaths": 1301,
            "country": "Yemen",
            "population": 28250420,
            "sq_km_area": 527968,
            "life_expectancy": "59.8",
            "elevation_in_meters": 999,
            "continent": "Asia",
            "abbreviation": "YE",
            "location": "Middle East",
            "iso": 887,
            "capital_city": "Sanaa",
            "lat": "15.552727",
            "long": "48.516388",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Zambia": {
        "All": {
            "confirmed": 92754,
            "recovered": 90955,
            "deaths": 1265,
            "country": "Zambia",
            "population": 17094130,
            "sq_km_area": 752618,
            "life_expectancy": "37.2",
            "elevation_in_meters": "1,138",
            "continent": "Africa",
            "abbreviation": "ZM",
            "location": "Eastern Africa",
            "iso": 894,
            "capital_city": "Lusaka",
            "lat": "-13.133897",
            "long": "27.849332",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Zimbabwe": {
        "All": {
            "confirmed": 38612,
            "recovered": 36416,
            "deaths": 1583,
            "country": "Zimbabwe",
            "population": 16529904,
            "sq_km_area": 390757,
            "life_expectancy": "37.8",
            "elevation_in_meters": 961,
            "continent": "Africa",
            "abbreviation": "ZW",
            "location": "Eastern Africa",
            "iso": 716,
            "capital_city": "Harare",
            "lat": "-19.015438",
            "long": "29.154857",
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "US": {
        "All": {
            "confirmed": 33041136,
            "recovered": 0,
            "deaths": 588185,
            "country": "US",
            "population": 324459463,
            "sq_km_area": 9363520,
            "life_expectancy": "77.1",
            "elevation_in_meters": 760,
            "continent": "North America",
            "abbreviation": "US",
            "location": "North America",
            "iso": 840,
            "capital_city": "Washington"
        },
        "Mississippi": {
            "lat": "32.741646",
            "long": "-89.678696",
            "confirmed": 316167,
            "recovered": 0,
            "deaths": 7278,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Grand Princess": {
            "lat": "37.64894",
            "long": "-122.665479",
            "confirmed": 103,
            "recovered": 0,
            "deaths": 3,
            "updated": "2020/08/04 02:27:56+00"
        },
        "Oklahoma": {
            "lat": "35.565342",
            "long": "-96.928917",
            "confirmed": 451645,
            "recovered": 0,
            "deaths": 6918,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Delaware": {
            "lat": "39.318523",
            "long": "-75.507141",
            "confirmed": 107928,
            "recovered": 0,
            "deaths": 1652,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Minnesota": {
            "lat": "45.694454",
            "long": "-93.900192",
            "confirmed": 597052,
            "recovered": 0,
            "deaths": 7426,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Illinois": {
            "lat": "40.349457",
            "long": "-88.986137",
            "confirmed": 1371791,
            "recovered": 0,
            "deaths": 24913,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Arkansas": {
            "lat": "34.969704",
            "long": "-92.373123",
            "confirmed": 339406,
            "recovered": 0,
            "deaths": 5805,
            "updated": "2021/05/20 18:21:02+00"
        },
        "New Mexico": {
            "lat": "34.840515",
            "long": "-106.248482",
            "confirmed": 201489,
            "recovered": 0,
            "deaths": 4122,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Indiana": {
            "lat": "39.849426",
            "long": "-86.258278",
            "confirmed": 738173,
            "recovered": 0,
            "deaths": 13525,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Louisiana": {
            "lat": "31.169546",
            "long": "-91.867805",
            "confirmed": 467475,
            "recovered": 0,
            "deaths": 10517,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Texas": {
            "lat": "31.054487",
            "long": "-97.563461",
            "confirmed": 2937788,
            "recovered": 0,
            "deaths": 51128,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Wisconsin": {
            "lat": "44.268543",
            "long": "-89.616508",
            "confirmed": 670876,
            "recovered": 0,
            "deaths": 7732,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kansas": {
            "lat": "38.5266",
            "long": "-96.726486",
            "confirmed": 314086,
            "recovered": 0,
            "deaths": 5054,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Recovered": {
            "lat": "0",
            "long": "0",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "2021/05/20 18:21:02+00"
        },
        "American Samoa": {
            "lat": "-14.271",
            "long": "-170.132",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "1970/01/01 00:00:00+00"
        },
        "Connecticut": {
            "lat": "41.597782",
            "long": "-72.755371",
            "confirmed": 345948,
            "recovered": 0,
            "deaths": 8204,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Virgin Islands": {
            "lat": "18.3358",
            "long": "-64.8963",
            "confirmed": 3308,
            "recovered": 0,
            "deaths": 27,
            "updated": "2021/05/20 18:21:02+00"
        },
        "California": {
            "lat": "36.116203",
            "long": "-119.681564",
            "confirmed": 3772933,
            "recovered": 0,
            "deaths": 62783,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Puerto Rico": {
            "lat": "18.2208",
            "long": "-66.5901",
            "confirmed": 137147,
            "recovered": 0,
            "deaths": 2461,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Georgia": {
            "lat": "33.040619",
            "long": "-83.643074",
            "confirmed": 1117475,
            "recovered": 0,
            "deaths": 20569,
            "updated": "2021/05/20 18:21:02+00"
        },
        "North Dakota": {
            "lat": "47.528912",
            "long": "-99.784012",
            "confirmed": 109440,
            "recovered": 0,
            "deaths": 1537,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Pennsylvania": {
            "lat": "40.590752",
            "long": "-77.209755",
            "confirmed": 1192678,
            "recovered": 0,
            "deaths": 26924,
            "updated": "2021/05/20 18:21:02+00"
        },
        "West Virginia": {
            "lat": "38.491226",
            "long": "-80.954453",
            "confirmed": 159450,
            "recovered": 0,
            "deaths": 2769,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Alaska": {
            "lat": "61.370716",
            "long": "-152.404419",
            "confirmed": 69756,
            "recovered": 0,
            "deaths": 369,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Tennessee": {
            "lat": "35.747845",
            "long": "-86.692345",
            "confirmed": 859281,
            "recovered": 0,
            "deaths": 12353,
            "updated": "2021/05/20 18:21:02+00"
        },
        "United States Virgin Islands": {
            "lat": "18.35",
            "long": "-64.933",
            "confirmed": 0,
            "recovered": 0,
            "deaths": 0,
            "updated": "1970/01/01 00:00:00+00"
        },
        "Missouri": {
            "lat": "38.456085",
            "long": "-92.288368",
            "confirmed": 601755,
            "recovered": 0,
            "deaths": 9406,
            "updated": "2021/05/20 18:21:02+00"
        },
        "South Dakota": {
            "lat": "44.299782",
            "long": "-99.438828",
            "confirmed": 123877,
            "recovered": 0,
            "deaths": 2001,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Colorado": {
            "lat": "39.059811",
            "long": "-105.311104",
            "confirmed": 534364,
            "recovered": 0,
            "deaths": 6472,
            "updated": "2021/05/20 18:21:02+00"
        },
        "New Jersey": {
            "lat": "40.298904",
            "long": "-74.521011",
            "confirmed": 1012196,
            "recovered": 0,
            "deaths": 26030,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Guam": {
            "lat": "13.4443",
            "long": "144.7937",
            "confirmed": 8124,
            "recovered": 0,
            "deaths": 139,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Washington": {
            "lat": "47.400902",
            "long": "-121.490494",
            "confirmed": 424903,
            "recovered": 0,
            "deaths": 5673,
            "updated": "2021/05/20 18:21:02+00"
        },
        "New York": {
            "lat": "42.165726",
            "long": "-74.948051",
            "confirmed": 2090803,
            "recovered": 0,
            "deaths": 53058,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Nevada": {
            "lat": "38.313515",
            "long": "-117.055374",
            "confirmed": 322018,
            "recovered": 0,
            "deaths": 5550,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Northern Mariana Islands": {
            "lat": "15.0979",
            "long": "145.6739",
            "confirmed": 176,
            "recovered": 0,
            "deaths": 2,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Diamond Princess": {
            "lat": "35.4437",
            "long": "139.638",
            "confirmed": 49,
            "recovered": 0,
            "deaths": 0,
            "updated": "2020/08/04 02:27:56+00"
        },
        "Maryland": {
            "lat": "39.063946",
            "long": "-76.802101",
            "confirmed": 457084,
            "recovered": 0,
            "deaths": 8959,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Idaho": {
            "lat": "44.240459",
            "long": "-114.478828",
            "confirmed": 190712,
            "recovered": 0,
            "deaths": 2074,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Wyoming": {
            "lat": "42.755966",
            "long": "-107.30249",
            "confirmed": 59376,
            "recovered": 0,
            "deaths": 713,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Arizona": {
            "lat": "33.729759",
            "long": "-111.431221",
            "confirmed": 874605,
            "recovered": 0,
            "deaths": 17497,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Iowa": {
            "lat": "42.011539",
            "long": "-93.210526",
            "confirmed": 370154,
            "recovered": 0,
            "deaths": 6025,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Michigan": {
            "lat": "43.326618",
            "long": "-84.536095",
            "confirmed": 982400,
            "recovered": 0,
            "deaths": 19931,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Utah": {
            "lat": "40.150032",
            "long": "-111.862434",
            "confirmed": 403418,
            "recovered": 0,
            "deaths": 2275,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Virginia": {
            "lat": "37.769337",
            "long": "-78.169968",
            "confirmed": 671916,
            "recovered": 0,
            "deaths": 11068,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Oregon": {
            "lat": "44.572021",
            "long": "-122.070938",
            "confirmed": 196788,
            "recovered": 0,
            "deaths": 2601,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Montana": {
            "lat": "46.921925",
            "long": "-110.454353",
            "confirmed": 111025,
            "recovered": 0,
            "deaths": 1604,
            "updated": "2021/05/20 18:21:02+00"
        },
        "New Hampshire": {
            "lat": "43.452492",
            "long": "-71.563896",
            "confirmed": 97978,
            "recovered": 0,
            "deaths": 1340,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Massachusetts": {
            "lat": "42.230171",
            "long": "-71.530106",
            "confirmed": 703292,
            "recovered": 0,
            "deaths": 17794,
            "updated": "2021/05/20 18:21:02+00"
        },
        "South Carolina": {
            "lat": "33.856892",
            "long": "-80.945007",
            "confirmed": 589846,
            "recovered": 0,
            "deaths": 9669,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Vermont": {
            "lat": "44.045876",
            "long": "-72.710686",
            "confirmed": 24026,
            "recovered": 0,
            "deaths": 255,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Florida": {
            "lat": "27.766279",
            "long": "-81.686783",
            "confirmed": 2299596,
            "recovered": 0,
            "deaths": 36271,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Hawaii": {
            "lat": "21.094318",
            "long": "-157.498337",
            "confirmed": 35585,
            "recovered": 0,
            "deaths": 492,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Kentucky": {
            "lat": "37.66814",
            "long": "-84.670067",
            "confirmed": 454044,
            "recovered": 0,
            "deaths": 6678,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Rhode Island": {
            "lat": "41.680893",
            "long": "-71.51178",
            "confirmed": 150948,
            "recovered": 0,
            "deaths": 2703,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Nebraska": {
            "lat": "41.12537",
            "long": "-98.268082",
            "confirmed": 222780,
            "recovered": 0,
            "deaths": 2240,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Ohio": {
            "lat": "40.388783",
            "long": "-82.764915",
            "confirmed": 1094742,
            "recovered": 0,
            "deaths": 19628,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Alabama": {
            "lat": "32.3182",
            "long": "-86.9023",
            "confirmed": 541230,
            "recovered": 0,
            "deaths": 11077,
            "updated": "2021/05/20 18:21:02+00"
        },
        "North Carolina": {
            "lat": "35.630066",
            "long": "-79.806419",
            "confirmed": 994734,
            "recovered": 0,
            "deaths": 12950,
            "updated": "2021/05/20 18:21:02+00"
        },
        "District of Columbia": {
            "lat": "38.897438",
            "long": "-77.026817",
            "confirmed": 48662,
            "recovered": 0,
            "deaths": 1125,
            "updated": "2021/05/20 18:21:02+00"
        },
        "Maine": {
            "lat": "44.693947",
            "long": "-69.381927",
            "confirmed": 66535,
            "recovered": 0,
            "deaths": 816,
            "updated": "2021/05/20 18:21:02+00"
        }
    },
    "Global": {
        "All": {
            "population": 7444509223,
            "confirmed": 165063672,
            "recovered": 101325056,
            "deaths": 3420957
        }
    }
}