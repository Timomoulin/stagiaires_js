
// Les Variables globales
let stagiaires = [{
        idStagiaire: 1,
        prenom: "elias",
        nom: "aggoune",
        email: "elias.aggoune@elias-agg.com"
    },
    {
        idStagiaire: 3,
        prenom: "mehdi",
        nom: "benabbou",
        email: "mehdibenabbou95@gmail.com"
    }, {
        idStagiaire: 4,
        prenom: "nouh",
        nom: "bensalem",
        email: "bensalem.noe@gmail.com"
    }, {
        idStagiaire: 5,
        prenom: "guillaume",
        nom: "boltz",
        email: "boltzguillaume@gmail.com"
    },
    {
        idStagiaire: 6,
        prenom: "yannick",
        nom: "bonnaud",
        email: "yannick1981@free.fr"
    },
    {
        idStagiaire: 7,
        prenom: "anthony",
        nom: "chaudey",
        email: "antcha.dev@gmail.com"
    },
    {
        idStagiaire: 8,
        prenom: "abdoulaye",
        nom: "coulibaly",
        email: "coulibaly.abdou@ymail.com"
    },
    {
        idStagiaire: 9,
        prenom: "grégoire",
        nom: "denis",
        email: "greg41denis@gmail.com"
    },
    {
        idStagiaire: 10,
        prenom: "victor",
        nom: "emilio sensua",
        email: "emiliosensuavictor@gmail.com"
    },
    {
        idStagiaire: 11,
        prenom: "nouara",
        nom: "ferhoune",
        email: "ferhounenouara@yahoo.fr"
    },
    {
        idStagiaire: 12,
        prenom: "florian",
        nom: "gagnant",
        email: "gagnantflorian@gmail.com"
    },
    {
        idStagiaire: 13,
        prenom: "faycal",
        nom: "hassaine",
        email: "faycal.hasn@gmail.com"
    },
    {
        idStagiaire: 14,
        prenom: "ouahiba",
        nom: "idjennaden",
        email: "ouahiba.idjennaden@gmail.com"
    },
    {
        idStagiaire: 15,
        prenom: "fabien",
        nom: "lecouve",
        email: "fabien.lecouve@hotmail.com"
    },
    {
        idStagiaire: 16,
        prenom: "louis",
        nom: "nsimba",
        email: "louis.nsimba@outlook.fr"
    },
    {
        idStagiaire: 17,
        prenom: "styven",
        nom: "ho-van-to",
        email: "styven.hovanto@gmail.com"
    }
];

//Fin des variables globales

//Les définitions de fonctions

/**
 * Exercice 8 :
 * Fonction qui permet d'afficher un stagiaire
 * @param {*} unStagiaire 
 * @returns 
 */
function afficheStagiaire(unStagiaire) {


    return `<div class="col-sm-3 my-2">
    <div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">${unStagiaire.nom.toUpperCase()}</h5>
      <h6 class="card-subtitle mb-2 text-muted">${unStagiaire.prenom}</h6>
      <a href="mailto:${unStagiaire.email}" class="card-link"> ${unStagiaire.email}</a>
    </div>
    </div>
    </div>
    `
}

/**
 * Exercice 9 :
 * Fonction qui permet d'afficher les stagiaires
 * utilise la fonction afficheStagiaire
 * @param {*} desStagiaires un array de stagiaires
 */
function afficheDesStagiaires(desStagiaires) {
    let chaine = "";
    for (let i = 0; i < desStagiaires.length; i++) {
        let leStagiaire = desStagiaires[i];
        chaine += afficheStagiaire(leStagiaire)
    }
    document.querySelector("#divStagiaires").innerHTML = chaine;
}

/**
 * Exercice 10 :
 * Fonction qui permet d'afficher seulement les stagiaires rechercher
 */
function rechercheStagiaire() {
    let query = document.querySelector("#rechercheStagiaire").value;
    let result = [];
    for (let i = 0; i < stagiaires.length; i++) {
        let stagiaire = stagiaires[i];
        if (stagiaire.prenom.toLowerCase().includes(query.toLowerCase())) {
            result.push(stagiaire);
        }
    }
    afficheDesStagiaires(result);
}

/**
 * Falcultatif :
 * Procedure qui permet de constituer le dataList de manière dynamique
 */
function afficheDataList() {
    let chaine = "";
    for (let i = 0; i < stagiaires.length; i++) {
        let stagiaire = stagiaires[i];
        chaine += `<option value="${stagiaire.prenom}">`;
    }
    document.querySelector("#dataListPrenom").innerHTML = chaine;
}

async function fetchJSON(url){
    let reponse= await fetch(url);
    let data= await reponse.json();
    console.log(data);
}
//Fin des définition de fonction

//Invocation de fonction
fetchJSON("https://restcountries.eu/rest/v2/all");
afficheDataList(stagiaires);

afficheDesStagiaires(stagiaires);

//Fin des invocation de fonction