<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stagiaire</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
</head>

<body>
    <?php
 include("./navbar.php");
 ?>

    <div class="container">
    <div class="row">
        <h1 class="m-3">Stagiaires JS</h1>
    </div>
        <div class="row d-flex m-3 justify-content-center">
            <div class="col-sm-6">
                <input id="rechercheStagiaire" list="dataListPrenom" onchange="rechercheStagiaire()" type="search" placeholder="Rechercher" class="form-control">
                <datalist id="dataListPrenom">
                
                </datalist>
            </div>
        </div>
        <div id="divStagiaires" class="row d-flex card-deck"></div>
    </div>

    <script src="js/script.js"></script>
</body>

</html>